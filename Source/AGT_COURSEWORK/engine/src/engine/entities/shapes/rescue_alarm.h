#pragma once

namespace engine
{
	class mesh;
	class rescue_alarm
	{
	public:

		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="vertices"></param>
		rescue_alarm(std::vector<glm::vec3> vertices);
		~rescue_alarm();

		/// <summary>
		/// Vertices of rescue_alarm mesh
		/// </summary>
		/// <returns></returns>
		std::vector<glm::vec3> vertices() const { return m_vertices; }

		/// <summary>
		/// Mesh object instance
		/// </summary>
		/// <returns></returns>
		ref<engine::mesh> mesh() const { return m_mesh; }

		/// <summary>
		/// Create rescue_alarm mesh based on vertices
		/// </summary>
		/// <param name="vertices"></param>
		/// <returns></returns>
		static ref<rescue_alarm> create(std::vector<glm::vec3> vertices);

	private:

		/// <summary>
		/// Vertices of rescue_alarm mesh
		/// </summary>
		/// <returns></returns>
		std::vector<glm::vec3> m_vertices;

		/// <summary>
		/// Mesh object instance
		/// </summary>
		/// <returns></returns>
		ref<engine::mesh> m_mesh;
	};
}


