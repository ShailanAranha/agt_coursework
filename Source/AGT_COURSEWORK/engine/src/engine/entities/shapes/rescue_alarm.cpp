#include "pch.h" 
#include "rescue_alarm.h" 
#include <engine.h>

/// <summary>
/// Default constructor
/// </summary>
/// <param name="vertices"></param>
engine::rescue_alarm::rescue_alarm(std::vector<glm::vec3> vertices) : m_vertices(vertices)
{

	std::vector<glm::vec3> normals;

	// -------------- BOX 1 ----------------

	


	// FRONT
	normals.push_back(glm::cross(vertices.at(0) - vertices.at(4), vertices.at(0) -
		vertices.at(1)));

	normals.push_back(glm::cross(vertices.at(1) - vertices.at(5), vertices.at(1) -
		vertices.at(4)));

	//RIGHT
	normals.push_back(glm::cross(vertices.at(1) - vertices.at(5), vertices.at(1) -
		vertices.at(2)));

	normals.push_back(glm::cross(vertices.at(2) - vertices.at(5), vertices.at(2) -
		vertices.at(6)));

	// BACK
	normals.push_back(glm::cross(vertices.at(2) - vertices.at(6), vertices.at(2) -
		vertices.at(7)));

	normals.push_back(glm::cross(vertices.at(2) - vertices.at(3), vertices.at(2) -
		vertices.at(7)));

	// LEFT

	normals.push_back(glm::cross(vertices.at(3) - vertices.at(0), vertices.at(3) -
		vertices.at(4)));

	normals.push_back(glm::cross(vertices.at(3) - vertices.at(4), vertices.at(3) -
		vertices.at(7)));

	// TOP

	normals.push_back(glm::cross(vertices.at(3) - vertices.at(0), vertices.at(3) -
		vertices.at(1)));

	normals.push_back(glm::cross(vertices.at(3) - vertices.at(1), vertices.at(3) -
		vertices.at(2)));

	// ------------------- ALAM BASE -----------------

	// left
	normals.push_back(glm::cross(vertices.at(10) - vertices.at(8), vertices.at(10) -
		vertices.at(9)));

	// right
	normals.push_back(glm::cross(vertices.at(13) - vertices.at(11), vertices.at(13) -
		vertices.at(12)));

	// front 
	normals.push_back(glm::cross(vertices.at(10) - vertices.at(9), vertices.at(10) -
		vertices.at(13)));

	normals.push_back(glm::cross(vertices.at(12) - vertices.at(9), vertices.at(12) -
		vertices.at(13)));

	// back
	normals.push_back(glm::cross(vertices.at(10) - vertices.at(13), vertices.at(10) -
		vertices.at(8)));

	normals.push_back(glm::cross(vertices.at(11) - vertices.at(8), vertices.at(11) -
		vertices.at(13)));

	// bot
	normals.push_back(glm::cross(vertices.at(12) - vertices.at(9), vertices.at(12) -
		vertices.at(11)));

	normals.push_back(glm::cross(vertices.at(8) - vertices.at(9), vertices.at(8) -
		vertices.at(11)));

	std::vector<mesh::vertex> rescue_alarm_vertices
	{

		 // FRONT

		 { vertices.at(0), normals.at(0), { 0.f, 0.f } },
		 { vertices.at(4), normals.at(0), { 1.f, 0.f } },
		 { vertices.at(1), normals.at(0), { 0.5f, 1.f } },


		 { vertices.at(4), normals.at(1), { 0.f, 0.f } },
		 { vertices.at(5), normals.at(1), { 1.f, 0.f } },
		 { vertices.at(1), normals.at(1), { 0.5f, 1.f } },

		 //RIGHT

		 { vertices.at(5), normals.at(2), { 0.f, 0.f } },
		 { vertices.at(2), normals.at(2), { 1.f, 0.f } },
		 { vertices.at(1), normals.at(2), { 0.5f, 1.f } },

		 { vertices.at(2), normals.at(3), { 0.f, 0.f } },
		 { vertices.at(5), normals.at(3), { 1.f, 0.f } },
		 { vertices.at(6), normals.at(3), { 0.5f, 1.f } },

		 // BACK

		  { vertices.at(7), normals.at(4), { 0.f, 0.f } },
		 { vertices.at(2), normals.at(4), { 1.f, 0.f } },
		 { vertices.at(6), normals.at(4), { 0.5f, 1.f } },

		 { vertices.at(2), normals.at(5), { 0.f, 0.f } },
		 { vertices.at(7), normals.at(5), { 1.f, 0.f } },
		 { vertices.at(3), normals.at(5), { 0.5f, 1.f } },

		 // LEFT

		 { vertices.at(3), normals.at(6), { 0.f, 0.f } },
		 { vertices.at(4), normals.at(6), { 1.f, 0.f } },
		 { vertices.at(0), normals.at(6), { 0.5f, 1.f } },


		 { vertices.at(4), normals.at(7), { 0.f, 0.f } },
		 { vertices.at(3), normals.at(7), { 1.f, 0.f } },
		 { vertices.at(7), normals.at(7), { 0.5f, 1.f } },

		 // TOP

		{ vertices.at(1), normals.at(8), { 0.f, 0.f } },
		{ vertices.at(3), normals.at(8), { 1.f, 0.f } },
		{ vertices.at(0), normals.at(8), { 0.5f, 1.f } },


		{ vertices.at(3), normals.at(9), { 0.f, 0.f } },
		{ vertices.at(1), normals.at(9), { 1.f, 0.f } },
		{ vertices.at(2), normals.at(9), { 0.5f, 1.f } },

		// ------------------- ALAM BASE -----------------

		// left
		{ vertices.at(10), normals.at(10), { 0.f, 0.f } },
		{ vertices.at(8), normals.at(10), { 1.f, 0.f } },
		{ vertices.at(9), normals.at(10), { 0.5f, 1.f } },

		// right
		{ vertices.at(11), normals.at(11), { 0.f, 0.f } },
		{ vertices.at(13), normals.at(11), { 1.f, 0.f } },
		{ vertices.at(12), normals.at(11), { 0.5f, 1.f } },

		// front 
		{ vertices.at(12), normals.at(12), { 0.f, 0.f } },
		{ vertices.at(10), normals.at(12), { 1.f, 0.f } },
		{ vertices.at(9), normals.at(12), { 0.5f, 1.f } },

		{ vertices.at(10), normals.at(13), { 0.f, 0.f } },
		{ vertices.at(12), normals.at(13), { 1.f, 0.f } },
		{ vertices.at(13), normals.at(13), { 0.5f, 1.f } },

		// back
		{ vertices.at(13), normals.at(14), { 0.f, 0.f } },
		{ vertices.at(8), normals.at(14), { 1.f, 0.f } },
		{ vertices.at(10), normals.at(14), { 0.5f, 1.f } },

		{ vertices.at(8), normals.at(15), { 0.f, 0.f } },
		{ vertices.at(13), normals.at(15), { 1.f, 0.f } },
		{ vertices.at(11), normals.at(15), { 0.5f, 1.f } },

		// bot
		{ vertices.at(11), normals.at(16), { 0.f, 0.f } },
		{ vertices.at(9), normals.at(16), { 1.f, 0.f } },
		{ vertices.at(8), normals.at(16), { 0.5f, 1.f } },

		{ vertices.at(9), normals.at(17), { 0.f, 0.f } },
		{ vertices.at(11), normals.at(17), { 1.f, 0.f } },
		{ vertices.at(12), normals.at(17), { 0.5f, 1.f } }

	};

	// indices
	const std::vector<uint32_t> rescue_alarm_indices
	{

	 0, 1, 2, //front

	 3,4,5,

	 6,7,8,

	 9,10,11,

	 12,13,14,

	 15,16,17,

	 18,19,20,

	 21,22,23,

	 24,25,26,

	 27,28,29,

	 30,31,32,

	 33,34,35,

	 36,37,38,

	 39,40,41,

	 42,43,44,

	 45,46,47,

	 48,49,50,

	 51,52,53

	};

	m_mesh = engine::mesh::create(rescue_alarm_vertices, rescue_alarm_indices);
}

engine::rescue_alarm::~rescue_alarm() {}

/// <summary>
/// Create rescue_alarm mesh based on vertices
/// </summary>
/// <param name="vertices"></param>
/// <returns></returns>
engine::ref<engine::rescue_alarm> engine::rescue_alarm::create(std::vector<glm::vec3> vertices)
{
	return std::make_shared<engine::rescue_alarm>(vertices);
}
