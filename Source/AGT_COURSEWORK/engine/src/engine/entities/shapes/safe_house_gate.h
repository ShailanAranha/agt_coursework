#pragma once

namespace engine
{
	class mesh;
	class safe_house_gate
	{
	public:

		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="vertices"></param>
		safe_house_gate(std::vector<glm::vec3> vertices);
		~safe_house_gate();

		/// <summary>
		/// Vertices of safe_house_gate mesh
		/// </summary>
		/// <returns></returns>
		std::vector<glm::vec3> vertices() const { return m_vertices; }

		/// <summary>
		/// Mesh object instance
		/// </summary>
		/// <returns></returns>
		ref<engine::mesh> mesh() const { return m_mesh; }

		/// <summary>
		/// Create safe_house_gate mesh based on vertices
		/// </summary>
		/// <param name="vertices"></param>
		/// <returns></returns>
		static ref<safe_house_gate> create(std::vector<glm::vec3> vertices);

	private:

		/// <summary>
		/// Vertices of safe_house_gate mesh
		/// </summary>
		/// <returns></returns>
		std::vector<glm::vec3> m_vertices;

		/// <summary>
		/// Mesh object instance
		/// </summary>
		/// <returns></returns>
		ref<engine::mesh> m_mesh;
	};
}


