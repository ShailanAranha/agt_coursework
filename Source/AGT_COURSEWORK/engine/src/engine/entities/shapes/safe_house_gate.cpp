#include "pch.h" 
#include "safe_house_gate.h" 
#include <engine.h>

/// <summary>
/// Default constructor
/// </summary>
/// <param name="vertices"></param>
engine::safe_house_gate::safe_house_gate(std::vector<glm::vec3> vertices) : m_vertices(vertices)
{

	std::vector<glm::vec3> normals;
	// face 1
	normals.push_back(glm::cross(vertices.at(0) - vertices.at(2), vertices.at(0) -
		vertices.at(1)));

	// face 2
	normals.push_back(glm::cross(vertices.at(0) - vertices.at(3), vertices.at(0) -
		vertices.at(2)));

	//// face 3
	normals.push_back(glm::cross(vertices.at(0) - vertices.at(4), vertices.at(0) -
		vertices.at(3)));

	//// face 4
	normals.push_back(glm::cross(vertices.at(0) - vertices.at(1), vertices.at(0) -
		vertices.at(4)));


	// FRONT
	normals.push_back(glm::cross(vertices.at(1) - vertices.at(5), vertices.at(1) -
		vertices.at(2)));

	normals.push_back(glm::cross(vertices.at(2) - vertices.at(6), vertices.at(2) -
		vertices.at(5)));

	//RIGHT
	normals.push_back(glm::cross(vertices.at(2) - vertices.at(6), vertices.at(2) -
		vertices.at(3)));

	normals.push_back(glm::cross(vertices.at(3) - vertices.at(6), vertices.at(3) -
		vertices.at(7)));

	// BACK
	normals.push_back(glm::cross(vertices.at(3) - vertices.at(7), vertices.at(3) -
		vertices.at(8)));

	normals.push_back(glm::cross(vertices.at(3) - vertices.at(4), vertices.at(3) -
		vertices.at(8)));

	// LEFT

	normals.push_back(glm::cross(vertices.at(4) - vertices.at(1), vertices.at(4) -
		vertices.at(5)));

	normals.push_back(glm::cross(vertices.at(4) - vertices.at(5), vertices.at(4) -
		vertices.at(8)));



	std::vector<mesh::vertex> safe_house_gate_vertices
	{
		//front face 1
		// position normal tex coord 
		 { vertices.at(0), normals.at(0), { 0.f, 0.f } },
		 { vertices.at(1), normals.at(0), { 1.f, 0.f } },
		 { vertices.at(2), normals.at(0), { 0.5f, 1.f } },


		 //front face 2
		// position normal tex coord 
		{ vertices.at(0), normals.at(1), { 0.f, 0.f } },
		 { vertices.at(2), normals.at(1), { 1.f, 0.f } },
		 { vertices.at(3), normals.at(1), { 0.5f, 1.f } },

		 //front face 3
		{ vertices.at(0), normals.at(2), { 0.f, 0.f } },
		 { vertices.at(3), normals.at(2), { 1.f, 0.f } },
		 { vertices.at(4), normals.at(2), { 0.5f, 1.f } },

		 //front face 4
		 { vertices.at(0), normals.at(3), { 0.f, 0.f } },
		 { vertices.at(4), normals.at(3), { 1.f, 0.f } },
		 { vertices.at(1), normals.at(3), { 0.5f, 1.f } },

		 // FRONT

		 { vertices.at(1), normals.at(4), { 0.f, 0.f } },
		 { vertices.at(5), normals.at(4), { 1.f, 0.f } },
		 { vertices.at(2), normals.at(4), { 0.5f, 1.f } },


		 { vertices.at(5), normals.at(5), { 0.f, 0.f } },
		 { vertices.at(6), normals.at(5), { 1.f, 0.f } },
		 { vertices.at(2), normals.at(5), { 0.5f, 1.f } },

		 //RIGHT

		 { vertices.at(6), normals.at(6), { 0.f, 0.f } },
		 { vertices.at(3), normals.at(6), { 1.f, 0.f } },
		 { vertices.at(2), normals.at(6), { 0.5f, 1.f } },

		 { vertices.at(3), normals.at(7), { 0.f, 0.f } },
		 { vertices.at(6), normals.at(7), { 1.f, 0.f } },
		 { vertices.at(7), normals.at(7), { 0.5f, 1.f } },

		 // BACK

		  { vertices.at(8), normals.at(8), { 0.f, 0.f } },
		 { vertices.at(3), normals.at(8), { 1.f, 0.f } },
		 { vertices.at(7), normals.at(8), { 0.5f, 1.f } },

		 { vertices.at(3), normals.at(9), { 0.f, 0.f } },
		 { vertices.at(8), normals.at(9), { 1.f, 0.f } },
		 { vertices.at(4), normals.at(9), { 0.5f, 1.f } },

		 // LEFT

		 { vertices.at(4), normals.at(10), { 0.f, 0.f } },
		 { vertices.at(5), normals.at(10), { 1.f, 0.f } },
		 { vertices.at(1), normals.at(10), { 0.5f, 1.f } },


		 { vertices.at(5), normals.at(11), { 0.f, 0.f } },
		 { vertices.at(4), normals.at(11), { 1.f, 0.f } },
		 { vertices.at(8), normals.at(11), { 0.5f, 1.f } }
	};

	// indices
	const std::vector<uint32_t> safe_house_gate_indices
	{
	 0, 1, 2, //front

	 3,4,5,

	 6,7,8,

	 9,10,11,

	 12,13,14,

	 15,16,17,

	 18,19,20,

	 21,22,23,

	 //back

	 24,25,26,
	 27,28,29,

	 //left

	30,31,32,
	33,34,35

	};

	m_mesh = engine::mesh::create(safe_house_gate_vertices, safe_house_gate_indices);
}

engine::safe_house_gate::~safe_house_gate() {}

/// <summary>
/// Create safe_house_gate mesh based on vertices
/// </summary>
/// <param name="vertices"></param>
/// <returns></returns>
engine::ref<engine::safe_house_gate> engine::safe_house_gate::create(std::vector<glm::vec3> vertices)
{
	return std::make_shared<engine::safe_house_gate>(vertices);
}
