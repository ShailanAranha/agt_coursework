// --------- Entry Point ---------------
#include "intro_game_layer.h"
#include "main_game_layer.h"
#include "testing_layer.h"
#include "engine/core/entry_point.h"
#include "engine/events/key_event.h"
#include <iostream>

/// <summary>
/// Total layers of the games
/// </summary>
enum class EGameLayer
{ 
    Intro = 0,
    MainGame = 1
};

class sandbox_app : public engine::application 
{ 
public:

    /// <summary>
    /// Default constructor
    /// </summary>
    sandbox_app() : m_currentGameLayer{ EGameLayer::Intro }, m_intro_game_layerRef{ new intro_game_layer{} }
    {
 
       push_layer(m_intro_game_layerRef);


     //  m_main_game_layerRef = new main_game_layer();
     //  push_layer(m_main_game_layerRef);
    } 

    /// <summary>
    /// Destructor
    /// </summary>
    ~sandbox_app() = default;

    /// <summary>
    /// Play button click check
    /// </summary>
    bool m_isPlayBtnClicked = false;

    /// <summary>
    /// Dispatch event
    /// </summary>
    /// <param name="event"></param>
    void on_event(engine::event& event) override
    { 
        application::on_event(event); 

        engine::event_dispatcher dispatcher(event); 
        // dispatch event on window X pressed 
        dispatcher.dispatch<engine::key_pressed_event>(BIND_EVENT_FN(sandbox_app::on_key_pressed));
    }

   /// <summary>
   /// Executes once a frame (Local script update)
   /// </summary>
   /// <param name="time_step"></param>
   void localUpdate(const engine::timestep& time_step) override
   {
      if (m_isPostPlaySequenceLaunched)
      {
          return;
      }

      if (m_isPlayBtnClicked)
       {
          if (m_frameCount > m_waitForFrames)
          {
              m_isPostPlaySequenceLaunched = true;
               postPlayButtonClickSequence();
          }
           m_frameCount++;
        }
        
   }
    
    /// <summary>
    /// On keyboard key pressed event
    /// </summary>
    /// <param name="event"></param>
    /// <returns></returns>
    bool on_key_pressed(engine::key_pressed_event& event) 
    {
        //std::cout << "event";

        if (event.event_type() == engine::event_type_e::key_pressed) 
        { 
            if (event.key_code() == engine::key_codes::KEY_ESCAPE) 
            { 
                application::exit();
            }

            if (event.key_code() == engine::key_codes::KEY_T)
            {
                 push_layer(m_main_game_layerRef);
                 pop_layer(m_intro_game_layerRef);
            }

            switch (m_currentGameLayer)
            {
            case EGameLayer::Intro:

                if (m_currentGameLayer == EGameLayer::Intro)
                {
                    if (event.key_code() == engine::key_codes::KEY_ENTER)
                    {
                        switch (m_intro_game_layerRef->getCurrentActiveButton())
                        {
                        case EIntroButtons::Play:

                            if (!m_isPlayBtnClicked)
                            {
                                m_intro_game_layerRef->ActivateLoadingWindow();
                                m_isPlayBtnClicked = true;
                                /*m_main_game_layerRef = new main_game_layer();
                                push_layer(m_main_game_layerRef);
                                pop_layer(m_intro_game_layerRef);*/
                            }

                            break;
                        case EIntroButtons::Controls:
                            m_intro_game_layerRef->ActivateControlWindow();
                            break;

                        case EIntroButtons::Quit:

                            application::exit();

                            break;
                        default:
                            std::cout << "GAME.CPP::ERROR::Type not found";
                            break;
                        }

                    }

                        if ((event.key_code() == engine::key_codes::KEY_BACKSPACE) &&
                            (m_intro_game_layerRef->getCurrentActiveWindow() == EWindow::Controls))
                        {
                         
                            m_intro_game_layerRef->ActivateMainWindow();
                        }
                }

                break;
            case EGameLayer::MainGame:
                break;
            default:

                std::cout << "GAME>CPP::ERROR:: Type not found";
                break;
            }
            //PYRO_TRACE("{0}", static_cast<char>(e.key_code())); 
        } 
        return false; 
    }

    /// <summary>
    /// On keyboard key released event
    /// </summary>
    /// <param name="event"></param>
    /// <returns></returns>
    bool on_key_released(engine::key_pressed_event& event)
    {
        if (event.event_type() == engine::event_type_e::key_released)
        {
           
        }
        return false;
    }

    /// <summary>
    /// Get current active layer of the game
    /// </summary>
    /// <returns></returns>
    EGameLayer getCurrentGameLayer(){ return m_currentGameLayer; }

private:
    // LAYERS

    /// <summary>
    /// Intro screen layer instance reference
    /// </summary>
    intro_game_layer* m_intro_game_layerRef;

    /// <summary>
    /// Main Game layer instance reference
    /// </summary>
    main_game_layer* m_main_game_layerRef;

    // LAYER NAME

    /// <summary>
    /// Current active layer of the game
    /// </summary>
    EGameLayer m_currentGameLayer;

    /// <summary>
    /// frame counter
    /// </summary>
    int m_frameCount = 0;

    /// <summary>
    /// Total number of frames to wait
    /// </summary>
    int m_waitForFrames = 10;

    /// <summary>
    /// Is post play sequnce launched?
    /// </summary>
    bool m_isPostPlaySequenceLaunched = false;

    /// <summary>
    /// Sequnce of task after play button is clicked
    /// </summary>
    void postPlayButtonClickSequence()
    {
      /*  m_main_game_layerRef = new main_game_layer();
        push_layer(m_main_game_layerRef);
        pop_layer(m_intro_game_layerRef);*/

        pop_layer(m_intro_game_layerRef);
        m_main_game_layerRef = new main_game_layer();
        push_layer(m_main_game_layerRef);
    }
}; 

/// <summary>
/// Initaites application
/// </summary>
/// <returns></returns>
engine::application* engine::create_application() 
{ 
    return new sandbox_app(); 
}


