#include "pch.h"
#include "engine/core/input.h"
#include "engine/key_codes.h"
#include "player.h"
#include <engine/mouse_buttons_codes.h>
#include <engine/events/key_event.h>


/// <summary>
/// Default contructor
/// </summary>
player::player() : m_walkSpeed(1.0f), m_timer(0.0f), m_runSpeed(5.0f) //, m_runSpeed(2.5f)
{
	//player::m_speed = 0.0f;
	//player::m_timer = 0.0f;
}

player::~player(){}

/// <summary>
/// Initializes thr player
/// </summary>
/// <param name="object"></param>
void player::Initialise(engine::ref<engine::game_object> object, engine::ref<engine::audio_manager>&  a_audio_manager)
{
	m_object = object;
	m_audio_manager = a_audio_manager;
	m_object->set_forward(glm::vec3(0.0f, 0.0f, -1.0f));
	//m_object->set_position(glm::vec3(0.0f, 0.54f, 10.0f));
	m_object->set_position(glm::vec3(0.0f, 0.54f, 95.0f));
	m_object->set_rotation_amount(atan2(m_object->forward().x, m_object->forward().z));
	m_object->animated_mesh()->set_default_animation(1);


	idle();
	m_isWalking = m_isRunning = m_isJumping = false;
	m_isIdle = true;

	SetHealth(100);

	m_audio_manager->play("player_walking");
	m_audio_manager->pause("player_walking");

	m_audio_manager->play("player_running");
	m_audio_manager->pause("player_running");
}

/// <summary>
/// Executes ones a frame
/// </summary>
/// <param name="time_step"></param>
void player::on_update(const engine::timestep& time_step, bool a_isGunPickedUp)
{
	/*glm::vec3 l_position = m_object->position() += m_object->forward() * m_speed * (float)time_step;
	m_object->set_position(l_position);

	m_object->set_rotation_amount(atan2(m_object->forward().x, m_object->forward().z));*/

	if (kick_done)
	{
		m_object->set_position(m_object->position() + m_object->velocity() * (float)time_step * m_fltSpeed);

		m_fltSpeed -= (float)time_step;
		if (m_fltSpeed <= 0.0f)
		{
			m_fltSpeed = 0.0f;
			kick_done = false;
		}
	}



	if (m_object->position().z < -4.0f)
	{
		m_hasPlayerEnteredSafeHouse = true;
	}

	if (a_isGunPickedUp)
	{
		if (engine::input::mouse_button_pressed_once(engine::mouse_button_codes::MOUSE_BUTTON_RIGHT))
		{
			if(!m_hasPlayerEnteredSafeHouse)
				m_isFirstPersonShooter = !m_isFirstPersonShooter;
		}

		// CALLED TO TRIGGER THE RELEASE EVENT
		if (engine::input::mouse_button_released(engine::mouse_button_codes::MOUSE_BUTTON_RIGHT))
		{

		}
	}

	
	if (m_isFirstPersonShooter)
	{
		m_audio_manager->pause("player_walking");
		m_audio_manager->pause("player_running");
		return;
	}

	if (engine::input::key_pressed(engine::key_codes::KEY_W) && !engine::input::key_pressed(engine::key_codes::KEY_LEFT_SHIFT))
	{
		glm::vec3 l_position = m_object->position() += m_object->forward() * m_walkSpeed * (float)time_step;
		m_object->set_position(l_position);

		m_object->set_rotation_amount(atan2(m_object->forward().x, m_object->forward().z));

		walking();

		m_isIdle = m_isRunning = m_isJumping = false;
		m_isWalking = true;

	}

	if (engine::input::key_pressed(engine::key_codes::KEY_W) && engine::input::key_pressed(engine::key_codes::KEY_LEFT_SHIFT))
	{
		glm::vec3 l_position = m_object->position() += m_object->forward() * m_runSpeed * (float)time_step;
		m_object->set_position(l_position);

		m_object->set_rotation_amount(atan2(m_object->forward().x, m_object->forward().z));

		running();

		m_isIdle = m_isWalking = m_isJumping = false;
		m_isRunning = true;

	}

	if (engine::input::key_released(engine::key_codes::KEY_W))
	{
		idle();

		m_isWalking = m_isRunning = m_isJumping = false;
		m_isIdle = true;

	}

	if (engine::input::key_pressed(engine::key_codes::KEY_S))
	{
		glm::vec3 l_position = m_object->position() -= m_object->forward() * m_walkSpeed * (float)time_step;
		m_object->set_position(l_position);

		m_object->set_rotation_amount(atan2(m_object->forward().x, m_object->forward().z));

		walking();

		m_isIdle = m_isRunning = m_isJumping = false;
		m_isWalking = true;
	}

	if (engine::input::key_released(engine::key_codes::KEY_S))
	{
		idle();

		m_isWalking = m_isRunning = m_isJumping = false;
		m_isIdle = true;
	}

	if (engine::input::key_pressed(engine::key_codes::KEY_A))
	{
		turn(1.0f * time_step);
	}

	if (engine::input::key_released(engine::key_codes::KEY_A))
	{
		idle();

		m_isWalking = m_isRunning = m_isJumping = false;
		m_isIdle = true;
	}

	if (engine::input::key_pressed(engine::key_codes::KEY_D))
	{
		turn(-1.0f * time_step);
	}

	if (engine::input::key_released(engine::key_codes::KEY_D))
	{
		idle();

		m_isWalking = m_isRunning = m_isJumping = false;
		m_isIdle = true;
	}

	if (engine::input::key_pressed(engine::key_codes::KEY_SPACE))
	{
		//jump();
	}

	


	if (engine::input::key_pressed(engine::key_codes::KEY_M))
	{
		running();
	}

	m_object->animated_mesh()->on_update(time_step);


	if (m_timer > 0.0f)
	{
		m_timer -= (float)time_step;
		if (m_timer < 0.0f)
		{
			m_object->animated_mesh()->switch_animation(false);
			m_object->animated_mesh()->switch_root_movement(false);
			m_object->animated_mesh()->switch_animation(m_object->animated_mesh()->default_animation());
			m_walkSpeed = 1.0f;
		}
	}
}

/// <summary>
/// Changes the camera angle to first person  (TODO::ADD CAMERA LERP AFTER MILESTONE 1)
/// </summary>
/// <param name="camera"></param>
void player::update_camera_to_third_person(engine::perspective_camera& camera)
{
	/*float A = 2.0f;
	float B = 3.0f;
	float C = 6.0f;

	glm::vec3 cam_pos = m_object->position() - glm::normalize(m_object->forward()) * B;
	cam_pos.y += A;

	glm::vec3 cam_look_at = m_object->position() + glm::normalize(m_object->forward()) * C;
	cam_look_at.y = 0.0f;

	camera.set_view_matrix(cam_pos, cam_look_at);*/

	float A = 2.f;
	float B = 3.f;
	float C = 9.f;

	glm::vec3 cam_pos = m_object->position() - glm::normalize(m_object->forward()) * B;
	cam_pos.y += A;

	glm::vec3 cam_look_at = m_object->position() + glm::normalize(m_object->forward()) * C;
	cam_look_at.y = 0.f;
	camera.set_view_matrix(cam_pos, cam_look_at);
}

/// <summary>
/// Changes the camera angle to third person (TODO::ADD CAMERA LERP AFTER MILESTONE 1)
/// </summary>
/// <param name="camera"></param>
void player::update_camera_to_first_person(engine::perspective_camera& camera)
{
	float A = 2.f;
	float B = 0.f;
	float C = 10.f;

	glm::vec3 cam_pos = m_object->position() - glm::normalize(m_object->forward()) * B;
	cam_pos.y += A;
	glm::vec3 cam_look_at = m_object->position() + glm::normalize(m_object->forward()) * C;
	cam_look_at.y = 0.f;
	camera.set_view_matrix(cam_pos, cam_look_at);
}

/// <summary>
/// Turns the player at provided angle
/// </summary>
/// <param name="angle"></param>
void player::turn(float angle)
{
	//m_object->set_forward(glm::rotate(m_object->forward(), angle, glm::vec3(0.0f, 1.0f, 0.0f)));
	//std::cout << "Angle " << angle;
	m_object->set_forward(glm::rotate(m_object->forward(), angle, glm::vec3(0.f, 1.f,
		0.f)));
}

/// <summary>
/// Sets player to jump animation
/// </summary>
void player::jump()
{
	//m_object->animated_mesh()->switch_root_movement(true);
	m_object->animated_mesh()->switch_root_movement(false);
	m_object->animated_mesh()->switch_animation(3);
	m_walkSpeed = 0.0f;
	m_timer = m_object->animated_mesh()->animations().at(3)->mDuration;
}

/// <summary>
/// Sets player to idle animation
/// </summary>
void player::idle()
{
	if (m_isIdle)
	{
		return;
	}

	m_object->animated_mesh()->switch_animation(1);
	m_audio_manager->pause("player_walking");
	m_audio_manager->pause("player_running");
}

/// <summary>
/// Sets player to walking animation
/// </summary>
void player::walking()
{
	if (m_isWalking)
	{
		return;
	}

	m_object->animated_mesh()->switch_animation(2);
	m_audio_manager->unpause("player_walking");
}

/// <summary>
/// Sets player to running animation
/// </summary>
void player::running()
{
	if (m_isRunning)
	{
		return;
	}

	m_object->animated_mesh()->switch_animation(4);
	m_audio_manager->unpause("player_running");
}

/// <summary>
/// Sets player health
/// </summary>
/// <param name="a_iPlayerHealth"></param>
void player::SetHealth(int a_iPlayerHealth)
{
	if (a_iPlayerHealth < 0)
	{
		return;
	}

	m_iPlayerHealth = a_iPlayerHealth;
}


/// <summary>
/// Sets player health
/// </summary>
/// <param name="a_iPlayerHealth"></param>
void player::EnemyHit()
{
	int l_temp = m_iPlayerHealth;
	l_temp -= 10;
	if (l_temp < 0)
	{
		return;
	}

	m_iPlayerHealth = l_temp;
	m_audio_manager->play("zombie_hit");
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
void player::on_event(engine::event& event)
{
	if (event.event_type() == engine::event_type_e::key_pressed)
	{
		auto& e = dynamic_cast<engine::key_pressed_event&>(event);
		if (e.key_code() == engine::key_codes::KEY_C)
		{
			m_isFirstPerson = !m_isFirstPerson;
		}

		// FOR TESTING
		if (e.key_code() == engine::key_codes::KEY_O)
		{
			int l_score = m_iPlayerHealth;
			SetHealth(--l_score);
		}
	}

}

/// <summary>
/// Pushes player backwards in the opposite direction of the player
/// </summary>
void player::PushBack(const engine::perspective_camera& camera)
{

	m_object->set_velocity(glm::vec3(0.f));
	m_object->set_acceleration(glm::vec3(0.f, 0.f, 0.f));
	m_object->set_torque(glm::vec3(0.f));
	m_object->set_rotation_amount(0.f);
	m_object->set_rotation_axis(glm::vec3(0.f, 1.f, 0.f));
	m_object->set_angular_velocity(glm::vec3(0.f));

	// Set the ball to the current camera position
	//m_object->set_position(camera.position());
	//m_object->set_velocity(10.0f * -camera.front_vector());

	//m_object->set_velocity(10.0f * -camera.front_vector());
	m_object->set_velocity(10.0f * - glm::vec3(camera.front_vector().x, 0, camera.front_vector().z));

	kick_done = true;
	m_fltSpeed = 1.0f;
}
