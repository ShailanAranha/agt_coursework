#include "game_hud.h"
#include "pch.h"
#include "quad.h"
#include <engine/mouse_buttons_codes.h>
#include <engine/events/key_event.h>
#include <string>

/// <summary>
/// Default constructor
/// </summary>
game_hud::game_hud(){}
game_hud::~game_hud(){}

/// <summary>
/// Initiatize object instance
/// </summary>
/// <param name="object"></param>
void game_hud::initialise(engine::ref<engine::text_manager>	a_text_manager)
{
	m_text_manager = a_text_manager;

	m_gun2 = { "assets/gui/HUD/Gun/gun1.png","assets/gui/HUD/Gun/gun1_not_selected.png" };
	m_gun1 = { "assets/gui/HUD/Gun/gun2.png","assets/gui/HUD/Gun/gun2_not_selected.png" };

	m_quad = quad::create(glm::vec2(0.3f, 0.1f));

	m_currentPlayerStatus = EPlayerStatus::LookingForSafeHouse;
	//m_gun1.m_isGunSelected = true;

}

/// <summary>
/// Renders the elements on the screen
/// </summary>
/// <param name="shader"></param>
void game_hud::on_render(const engine::ref<engine::shader>& shader, bool a_isFirstPersonShooter, bool a_hasPlayerEnteredSafeHouse, int a_iPlayerHealth, bool a_isZombieWaveActive)
{
	if (a_isFirstPersonShooter)
	{
		return;
	}

	if (m_gun1.m_isGunPickedUp && !a_hasPlayerEnteredSafeHouse)
	{
		if (m_gun1.m_isGunSelected)
		{
			Add_UI_Element(shader, glm::vec3(1.3f, 0.8f, 0.1f), m_gun1.gun_texture_selected(), m_quad);
		}
		else
		{
			Add_UI_Element(shader, glm::vec3(1.3f, 0.8f, 0.1f), m_gun1.gun_texture_unselected(), m_quad);
		}
	}

	if (m_gun2.m_isGunPickedUp && !a_hasPlayerEnteredSafeHouse)
	{
		if (m_gun2.m_isGunSelected)
		{
			Add_UI_Element(shader, glm::vec3(1.3f, 0.6f, 0.1f), m_gun2.gun_texture_selected(), m_quad);
		}
		else
		{
			Add_UI_Element(shader, glm::vec3(1.3f, 0.6f, 0.1f), m_gun2.gun_texture_unselected(), m_quad);
		}
	}

	const auto text_shader = engine::renderer::shaders_library()->get("text_2D");

	std::string l_strHealthValue{ std::to_string(a_iPlayerHealth) };
	std::string l_strHealthPrefix{ "HEALTH :" };
	std::string l_strHealthPostfix{ "%" };
	std::string l_strDisplayHealth{l_strHealthPrefix+l_strHealthValue+l_strHealthPostfix};

	m_text_manager->render_text(text_shader, l_strDisplayHealth, 10.f, (float)engine::application::window().height() - 25.f, 0.5f, glm::vec4(1.f, 0.5f, 0.f, 1.f));

	if (a_isZombieWaveActive && !a_hasPlayerEnteredSafeHouse)
	{
		m_text_manager->render_text(text_shader, ZombieWaveDisplayMessage, 500.0f, (float)engine::application::window().height() -25.f, 0.5f, glm::vec4(1.f, 0.5f, 0.f, 1.f));
	}

	if (m_is_distance_meter_visible)
	{
		//std::string l_strDisplayText = l_strDisplayText.append("DISTANCE TO SAFE HOUSE : ").append("").append("M");
		std::string l_strDisplayText{};

		switch (m_currentPlayerStatus)
		{
		case EPlayerStatus::LookingForSafeHouse:

			if (m_iDistanceFromSafeHouse > 0)
			{
				std::string l_strValue{ std::to_string(m_iDistanceFromSafeHouse) };
				std::string l_strUnits{ "M" };
				l_strDisplayText = "DISTANCE TO SAFE HOUSE :" + l_strValue + l_strUnits;
			}
			else
			{
				m_currentPlayerStatus = EPlayerStatus::OutsideSafeHouse;
			}

			break;
		case EPlayerStatus::OutsideSafeHouse:

			l_strDisplayText = "SAFE HOUSE IS HERE, RAISE THE ALARM TO SIGNAL THE LEADER TO OPEN THE GATE";

			break;

		case EPlayerStatus::AtTheRescueAlarm:
			l_strDisplayText = "CLICK ON F TO TRIGGER THE ALARM";

			break;
		case EPlayerStatus::TriggeredRescueAlarm:
			l_strDisplayText = "CROSS THE SAFE HOUSE GATE";
			break;
		case EPlayerStatus::EnteredTheSafeHouse:
			l_strDisplayText = "CONGRATULATIONS!! YOU ARE SAFE NOW, ENTER THE SAFE HOUSE";

			break;
		default:
			std::cout << "ERROR::GAMEHUD:: TRYEP NOIT FOUND!!!";
			break;
		}

		m_text_manager->render_text(text_shader, l_strDisplayText, 10.f, 25.0f, 0.5f, glm::vec4(1.f, 0.5f, 0.f, 1.f));
	}
}

/// <summary>
/// Executes once a frame
/// </summary>
/// <param name="time_step"></param>
void game_hud::on_update(const engine::timestep& time_step)
{
	if (engine::input::mouse_button_pressed_once(engine::mouse_button_codes::MOUSE_BUTTON_MIDDLE))
	{
		m_gun1.m_isGunSelected = !m_gun1.m_isGunSelected;
		m_gun2.m_isGunSelected = !m_gun2.m_isGunSelected;
	}

	// CALLED TO TRIGGER THE RELEASE EVENT
	if (engine::input::mouse_button_released(engine::mouse_button_codes::MOUSE_BUTTON_MIDDLE))
	{

	}

}

/// <summary>
/// Current gun selected by the player to shoot
/// </summary>
EGunType game_hud::CurrentGunSelected()
{
	if (m_gun1.m_isGunSelected)
	{
		return EGunType::Gun1;
	}

	return EGunType::Gun2;
}


/// <summary>
/// Update distnace info on UI
/// </summary>
void game_hud::updateDistanceFromSafeHouse(int a_iDistance)
{
	m_iDistanceFromSafeHouse = a_iDistance;
}

/// <summary>
/// Change player status based on the current player situation in the game
/// </summary>
void game_hud::ChangeStatus(EPlayerStatus a_status)
{
	m_currentPlayerStatus = a_status;
}


#pragma region HELPER FUNCTIONS

/// <summary>
///  Adds the texture to the quad at the desired tranform and renders in 2D camera
/// </summary>
/// <param name="a_mesh_shader_2d"></param>
/// <param name="a_position"></param>
/// <param name="a_texture2d"></param>
/// <param name="a_quad"></param>
void game_hud::Add_UI_Element(const engine::ref<engine::shader> a_mesh_shader_2d, glm::vec3 a_position, engine::ref<engine::texture_2d> a_texture2d, engine::ref<quad> a_quad)
{
	glm::mat4 transform(1.0f);
	transform = glm::translate(transform, a_position);
	std::dynamic_pointer_cast<engine::gl_shader>(a_mesh_shader_2d)->set_uniform("transparency", 1.0f);
	std::dynamic_pointer_cast<engine::gl_shader>(a_mesh_shader_2d)->set_uniform("has_texture", true);
	a_texture2d->bind();
	engine::renderer::submit(a_mesh_shader_2d, a_quad->mesh(), transform);
}

#pragma endregion


/// <summary>
/// Renders the elements on the screen
/// </summary>
void game_hud::on_event(engine::event& event)
{
	if (event.event_type() == engine::event_type_e::key_pressed)
	{
		auto& e = dynamic_cast<engine::key_pressed_event&>(event);
		if (e.key_code() == engine::key_codes::KEY_H)
		{
			m_is_distance_meter_visible = !m_is_distance_meter_visible;
		}
	}

}

