#include "intro_game_layer.h"
#include "platform/opengl/gl_shader.h"

#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp>
#include "engine/events/key_event.h"
#include "engine/utils/track.h"

#include "quad.h"

/// <summary>
/// Default constructor
/// </summary>
intro_game_layer::intro_game_layer():  m_2d_camera(-1.6f, 1.6f, -0.9f, 0.9f)
{
	engine::application::window().hide_mouse_cursor();


	// ---------------- INITIAZIE AUDIO MANAGER AND LOAD AUDIOS (TODO:: AFTER MILESTONE 1) ---------

	// Initialise audio and play background music
	m_audio_manager = engine::audio_manager::instance();
	m_audio_manager->init();
	m_audio_manager->load_sound("assets/audio/button_click.mp3", engine::sound_type::track, BUTTON_SELECTED_ID);  // Royalty free music from https://www.zapsplat.com/sound-effect-category/button-clicks/
	m_audio_manager->load_sound("assets/audio/button_click.mp3", engine::sound_type::track, BUTTON_CLICK_ID);  // Royalty free music from https://www.zapsplat.com/sound-effect-category/button-clicks/
	m_audio_manager->load_sound("assets/audio/intro_sound.mp3", engine::sound_type::track, INTRO_BG);  // Royalty free music from  https://www.bensound.com/royalty-free-music/cinematic

	m_audio_manager->loop(BUTTON_SELECTED_ID, false); 
	m_audio_manager->loop(BUTTON_CLICK_ID, false);
	m_audio_manager->loop(INTRO_BG, true);

	m_audio_manager->play(INTRO_BG);

	// Initialise the shaders, materials and lights
	auto mesh_shader = engine::renderer::shaders_library()->get("mesh");
	auto text_shader = engine::renderer::shaders_library()->get("text_2D");

	m_directionalLight.Color = glm::vec3(1.0f, 1.0f, 1.0f);
	m_directionalLight.AmbientIntensity = 1.f;
	m_directionalLight.DiffuseIntensity = 1.f;
	m_directionalLight.Direction = glm::normalize(glm::vec3(1.0f, -1.0f, 0.0f));

	// set color texture unit
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->bind();
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("lighting_on", true);
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("gColorMap", 0);
	m_directionalLight.submit(mesh_shader);
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("gMatSpecularIntensity", 1.f);
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("gSpecularPower", 10.f);
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("transparency", 1.0f);

	std::dynamic_pointer_cast<engine::gl_shader>(text_shader)->bind();
	std::dynamic_pointer_cast<engine::gl_shader>(text_shader)->set_uniform("projection",
		glm::ortho(0.f, (float)engine::application::window().width(), 0.f,
			(float)engine::application::window().height()));


#pragma region OVERLAY
	/* Asset references :
	a) Introand controls screen background : https://www.maxpixel.net/Sci-fi-Fantasy-Background-Blue-Space-Nebula-Light-4111275
	b.) Mouse Icon : https://freesvg.org/mouse-icon
Note: The rest of the assets were created by me */

	m_intro_bg_texture = engine::texture_2d::create("assets/gui/IntroNRestart/bg/intro_bg.png", true);
	m_controls_bg_texture = engine::texture_2d::create("assets/gui/IntroNRestart/bg/control_bg.png", true);
	m_loading_bg_texture = engine::texture_2d::create("assets/gui/IntroNRestart/Loading/loading_bg.png", true);
	// TODO:: IMPLEMENT LOADING WHEEL (AFTER MILESTONE 1)
	//m_loading_wheel_texture = engine::texture_2d::create("assets/gui/IntroNRestart/Loading/loading_wheel.png", true);
	
	m_play_normal_state = engine::texture_2d::create("assets/gui/IntroNRestart/buttons/play_normal.png", true);
	m_play_highlighted_state = engine::texture_2d::create("assets/gui/IntroNRestart/buttons/play_highlighted.png", true);
	m_controls_normal_state = engine::texture_2d::create("assets/gui/IntroNRestart/buttons/controls_normal.png", true);
	m_controls_highlighted_state = engine::texture_2d::create("assets/gui/IntroNRestart/buttons/controls_highlighted.png", true);
	m_quit_normal_state = engine::texture_2d::create("assets/gui/IntroNRestart/buttons/quit_normal.png", true);
	m_quit_highlighted_state = engine::texture_2d::create("assets/gui/IntroNRestart/buttons/quit_highlighted.png", true);

	m_quad = quad::create(glm::vec2(1.6f, 0.9f));

	m_buttonQuad = quad::create(glm::vec2(0.3f, 0.1f));

	// TODO:: IMPLEMENT LOADING WHEEL (AFTER MILESTONE 1)
	//m_loadingQuad = quad::create(glm::vec2(0.08f, 0.08f)); 

	m_currentActiveButton = EIntroButtons::Play;  // active button
	m_currentOpenWindow = EWindow::Main;

#pragma endregion

	m_text_manager = engine::text_manager::create();

}

intro_game_layer::~intro_game_layer() {}


/// <summary>
/// Executes ones a frame
/// </summary>
/// <param name="time_step"></param>
void intro_game_layer::on_update(const engine::timestep& time_step)
{
	//m_audio_manager->update_with_camera(m_3d_camera);
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
void intro_game_layer::on_render()
{
	//engine::render_command::clear_color({ 0.2f, 0.3f, 0.3f, 1.0f });
	//engine::render_command::clear_color({ 0.0f, 0.0f, 0.0f, 0.0f });
	engine::render_command::clear();

	const auto mesh_shader_2d = engine::renderer::shaders_library()->get("mesh");
	engine::renderer::begin_scene(m_2d_camera, mesh_shader_2d);

	switch (m_currentOpenWindow)
	{
	case EWindow::Main:
		OpenMainWindow(mesh_shader_2d);
		break;

	case EWindow::Controls:
		OpenControlsWindow(mesh_shader_2d);
		break;

	case EWindow::Loading:
		OpenLoadingWindow(mesh_shader_2d);
		break;

	default:

		std::cout << "INTRO_GAME_LAYER::ERROR:: Type not found";
		break;
	}

	/*glm::mat4 transform(1.0f);
	transform = glm::translate(transform, glm::vec3(0.f, 0.f, 0.1f));
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader_2d)->set_uniform("transparency", 1.0f);
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader_2d)->set_uniform("has_texture", true);
	m_intro_bg_texture->bind();
	engine::renderer::submit(mesh_shader_2d, m_quad->mesh(), transform);*/

	engine::renderer::end_scene();

	// Render text
	const auto text_shader = engine::renderer::shaders_library()->get("text_2D");
	m_text_manager->render_text(text_shader, "Orange Text", 10.f, (float)engine::application::window().height() - 25.f, 0.5f, glm::vec4(1.f, 0.5f, 0.f, 1.f));


}

/// <summary>
/// Event delegate 
/// </summary>
/// <param name="event"></param>
void intro_game_layer::on_event(engine::event& event)
{
	if (event.event_type() == engine::event_type_e::key_pressed)
	{
		auto& e = dynamic_cast<engine::key_pressed_event&>(event);
		if (e.key_code() == engine::key_codes::KEY_W)
		{
			int l_iCurrentActiveButtonValue = (int)m_currentActiveButton;

			if (!(l_iCurrentActiveButtonValue <= 0))
			{
				--l_iCurrentActiveButtonValue;
				m_currentActiveButton = (EIntroButtons)l_iCurrentActiveButtonValue;
				m_audio_manager->play(BUTTON_SELECTED_ID);
			}
		}

		if (e.key_code() == engine::key_codes::KEY_S)
		{
			int l_iCurrentActiveButtonValue = (int)m_currentActiveButton;

			if (!(l_iCurrentActiveButtonValue >= 2))
			{
				++l_iCurrentActiveButtonValue;
				m_currentActiveButton = (EIntroButtons)l_iCurrentActiveButtonValue;
				m_audio_manager->play(BUTTON_SELECTED_ID);
			}
		}
	}

	if (event.event_type() == engine::event_type_e::key_released)
	{
		auto& e = dynamic_cast<engine::key_released_event&>(event);
		/*if (e.key_code() == engine::key_codes::KEY_TAB)
		{
			engine::render_command::toggle_wireframe();
		}*/
	}
}

#pragma region ACTIVATE WINDOW

/// <summary>
/// Activates main intro screen
/// </summary>
void intro_game_layer::ActivateMainWindow()
{
	m_currentOpenWindow = EWindow::Main;
	m_audio_manager->play(BUTTON_CLICK_ID);
}

/// <summary>
/// Activates control window scren
/// </summary>
void intro_game_layer::ActivateControlWindow()
{
	m_currentOpenWindow = EWindow::Controls;
	m_audio_manager->play(BUTTON_CLICK_ID);
}

/// <summary>
/// Activates loading window scren
/// </summary>
void intro_game_layer::ActivateLoadingWindow()
{
	m_currentOpenWindow = EWindow::Loading;
	m_audio_manager->play(BUTTON_CLICK_ID);
	m_audio_manager->pause(INTRO_BG);
}

#pragma endregion


#pragma region HELPER FUNCTIONS

/// <summary>
///  Adds the texture to the quad at the desired tranform and renders in 2D camera
/// </summary>
/// <param name="a_mesh_shader_2d"></param>
/// <param name="a_position"></param>
/// <param name="a_texture2d"></param>
/// <param name="a_quad"></param>
void intro_game_layer::Add_UI_Element(const engine::ref<engine::shader> a_mesh_shader_2d, glm::vec3 a_position, engine::ref<engine::texture_2d> a_texture2d, engine::ref<quad> a_quad)
{
	glm::mat4 transform(1.0f);
	transform = glm::translate(transform, a_position);
	std::dynamic_pointer_cast<engine::gl_shader>(a_mesh_shader_2d)->set_uniform("transparency", 1.0f);
	std::dynamic_pointer_cast<engine::gl_shader>(a_mesh_shader_2d)->set_uniform("has_texture", true);
	a_texture2d->bind();
	engine::renderer::submit(a_mesh_shader_2d, a_quad->mesh(), transform);
}

/// <summary>
/// Opens Main intro screen
/// </summary>
/// <param name="a_mesh_shader_2d"></param>
void intro_game_layer::OpenMainWindow(engine::ref<engine::shader> a_mesh_shader_2d)
{
	switch (m_currentActiveButton)
	{
	case EIntroButtons::Play:
		Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, 0.f, 0.1f), m_play_highlighted_state, m_buttonQuad);
		Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, -0.2f, 0.1f), m_controls_normal_state, m_buttonQuad);
		Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, -0.4f, 0.1f), m_quit_normal_state, m_buttonQuad);
		break;
	case EIntroButtons::Controls:
		Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, 0.f, 0.1f), m_play_normal_state, m_buttonQuad);
		Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, -0.2f, 0.1f), m_controls_highlighted_state, m_buttonQuad);
		Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, -0.4f, 0.1f), m_quit_normal_state, m_buttonQuad);
		break;
	case EIntroButtons::Quit:
		Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, 0.f, 0.1f), m_play_normal_state, m_buttonQuad);
		Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, -0.2f, 0.1f), m_controls_normal_state, m_buttonQuad);
		Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, -0.4f, 0.1f), m_quit_highlighted_state, m_buttonQuad);
		break;
	default:
		std::cout << "INTRO_GAME_LAYER::ERROR::Type not found";
		break;
	}

	Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, 0.f, 0.1f), m_intro_bg_texture, m_quad);
}

/// <summary>
/// Opens controls window of intro screen
/// </summary>
/// <param name="a_mesh_shader_2d"></param>
void intro_game_layer::OpenControlsWindow(engine::ref<engine::shader> a_mesh_shader_2d)
{
	Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, 0.f, 0.1f), m_controls_bg_texture, m_quad);
}

/// <summary>
/// Opens loading window of intro screen
/// </summary>
/// <param name="a_mesh_shader_2d"></param>
void intro_game_layer::OpenLoadingWindow(engine::ref<engine::shader> a_mesh_shader_2d)
{
	// TODO:: IMPLEMENT LOADING WHEEL (AFTER MILESTONE 1)
	//Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, -0.4f, 0.1f), m_loading_wheel_texture, m_loadingQuad);
	Add_UI_Element(a_mesh_shader_2d, glm::vec3(0.f, 0.f, 0.1f), m_loading_bg_texture, m_quad);
}

#pragma endregion



