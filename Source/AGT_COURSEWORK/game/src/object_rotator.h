//HEADER GUARD
#ifndef OBJECT_ROTATOR
#define OBJECT_ROTATOR

#include <engine.h>

#endif

class object_rotator
{
public:
	/// <summary>
	/// Default constructor
	/// </summary>
	object_rotator() {}
	object_rotator(engine::ref < engine::game_object> a_obj, std::string a_strId, bool a_isRotate) :m_strId(a_strId), m_isRotate(a_isRotate), m_obj(a_obj)
	{

	}


	/// <summary>
	/// Game object Id 
	/// </summary>
	/// <returns></returns>
	std::string Id() { return m_strId; }

	/// <summary>
	/// Game object instance
	/// </summary>
	engine::ref<engine::game_object> m_obj;

	/// <summary>
	/// Rotate object??
	/// </summary>
	bool m_isRotate = false;

private:
	/// <summary>
	/// Game object Id 
	/// </summary>
	std::string m_strId = "";

};
