#include "pickup.h"

pickup::pickup(const engine::game_object_properties props) : engine::game_object(props)
{

}

pickup::~pickup()
{}
void pickup::init()
{
	m_isActive = true;
}
void pickup::update(glm::vec3 c, float dt)
{
	set_rotation_amount(rotation_amount() + dt * 1.5f);

	glm::vec3 l_vec3Distance = position() - c;

	//float l_fltDistance = position - c;

	//if (l_fltDistance > 1)


	//std::cout << "X:" << l_vec3Distance.x << " Y:" << l_vec3Distance.y << " Z:" << l_vec3Distance.z << '\n';
	//if(l_vec3Distance.x <1 || l_vec3Distance.y<1|| l_vec3Distance.z<1)


	//std::cout << glm::length(l_vec3Distance) << '\n';

	if (glm::length(l_vec3Distance) > 5)
	{
		m_isActive = true;
	}
	else
	{
		m_isActive = false;
	}

}
engine::ref<pickup> pickup::create(const engine::game_object_properties& props)
{
	return std::make_shared<pickup>(props);
}
