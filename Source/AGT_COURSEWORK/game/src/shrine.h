//HEADER GUARD
#ifndef SHRINE
#define SHRINE

#include <engine.h>

#endif

class shrine
{
public:

	/// <summary>
	/// Default contructor
	/// </summary>
	shrine();
	~shrine();

	/// <summary>
	/// Initializes the object
	/// </summary>
	/// <param name="object"></param>
	void initialise(engine::ref<engine::game_object> object);

	/// <summary>
	/// Executes ones a frame
	/// </summary>
	/// <param name="time_step"></param>
	void on_update(const engine::timestep& time_step);

	/// <summary>
	/// Renders the elements on the screen
	/// </summary>
	/// <param name="shader"></param>
	void on_render(const engine::ref<engine::shader>& shader, glm::mat4 l_shrine_tranform,
		engine::ref<engine::material> a_lightsource_material, engine::SpotLight a_spotLight,
		uint32_t a_num_spot_lights, int l_iLightNum);

	/// <summary>
	/// Renders the elements on the screen
	/// </summary>
	/// <param name="shader"></param>
	void shrine::on_event(engine::event& event);

	/// <summary>
	/// Set discharge properties of shire
	/// </summary>
	void shireUsed();

	/// <summary>
	/// Is shire used? (discharged)
	/// </summary>
	/// <returns></returns>
	bool IsShrineUsed() { return m_isShrineUsed; };

private:

	/// <summary>
	/// Object instance
	/// </summary>
	engine::ref<engine::game_object> m_object;

	/// <summary>
	/// Time left for the next shire cycle
	/// </summary>
	float m_fltTimer{ 0.0f };

	/// <summary>
	/// Time reqquired to charge the shire
	/// </summary>
	float m_fltTimeForNextShrine{ 10.0f };

	/// <summary>
	/// Is shire used? (discharged)
	/// </summary>
	bool m_isShrineUsed{ false };
};
