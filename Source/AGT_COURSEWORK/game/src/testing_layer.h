//HEADER GUARD

#ifndef TESTING_LAYER
#define TESTING_LAYER

#include <engine.h>
#include "player.h"

#endif 

class testing_layer : public engine::layer
{
public:
    testing_layer();
    ~testing_layer();

    void on_update(const engine::timestep& time_step) override;
    void on_render() override;
    void on_event(engine::event& event) override;


private:

    // CAMERAS
    engine::orthographic_camera m_2d_camera;
    engine::perspective_camera m_3d_camera;

    // ENVIRONMENT
    engine::ref<engine::skybox> m_skybox{};
    engine::ref<engine::game_object> m_terrain{};
    engine::DirectionalLight m_directionalLight;

    std::vector<engine::ref<engine::game_object>> m_game_objects{};


    //MANAGERS
    engine::ref<engine::audio_manager>  m_audio_manager{};

    engine::ref<engine::bullet_manager> m_physics_manager{};
    engine::ref<engine::text_manager>	m_text_manager{};


    // CHARACTER
    engine::ref<engine::game_object>	m_mannequin{};
    engine::ref<engine::material>		m_material{};
    engine::ref<engine::material>		m_mannequin_material{};
    player m_player{};

};


