//HEADER GUARD
#ifndef RESCUE_ALARM_HANDLER
#define RESCUE_ALARM_HANDLER

#include <engine.h>

#endif

class rescue_alarm_handler
{
public:

	/// <summary>
	/// Default contructor
	/// </summary>
	rescue_alarm_handler();
	~rescue_alarm_handler();

	/// <summary>
	/// Initializes the object
	/// </summary>
	/// <param name="object"></param>
	void initialise(engine::ref<engine::game_object> object);

	/// <summary>
	/// Executes ones a frame
	/// </summary>
	/// <param name="time_step"></param>
	void on_update(const engine::timestep& time_step);

	/// <summary>
	/// Renders the elements on the screen
	/// </summary>
	/// <param name="shader"></param>
	void on_render(const engine::ref<engine::shader>& shader, glm::mat4 l_rescue_alarm_handler_tranform,
		engine::ref<engine::material> a_lightsource_material, engine::SpotLight a_spotLight,
		uint32_t a_num_spot_lights, int l_iLightNum);

	/// <summary>
	/// Renders the elements on the screen
	/// </summary>
	/// <param name="shader"></param>
	void on_event(engine::event& event);

	/// <summary>
	/// Rasie Alarm
	/// </summary>
	void TriggerAlarm();

	/// <summary>
	/// Is Alarm Raised by the player?
	/// </summary>
	bool IsRescueAlarmTriggered() { return m_isrescue_alarm_triggered; };

private:

	/// <summary>
	/// Object instance
	/// </summary>
	engine::ref<engine::game_object> m_object;

	/// <summary>
	/// Is Alarm Raised by the player?
	/// </summary>
	bool m_isrescue_alarm_triggered{ false };

	/// <summary>
	/// Spotlight material
	/// </summary>
	engine::ref<engine::material> m_lightsource_material_spot_light_rescue_alarm;

	/// <summary>
	/// Update counter
	/// </summary>
	float m_fltCounter = 0.0f;

	/// <summary>
	/// Alarn glow speed
	/// </summary>
	float m_fltSpeed = 1.5f;
};
