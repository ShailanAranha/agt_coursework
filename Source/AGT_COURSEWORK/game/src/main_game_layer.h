//HEADER GUARD
#ifndef MAIN_GAME_LAYER
#define MAIN_GAME_LAYER

#include <engine.h>
#include "pickup.h"
#include "player.h"
#include "ballistic.h"
#include "game_hud.h"
#include "object_rotator_handler.h"
#include "bounding_box.h"
#include "safe_house_handler.h"
#include "shrine.h"
#include "rescue_alarm_handler.h"
#include "zombie_handler.h"

#endif


class pickup;
class quad;
class object_rotator;

class main_game_layer : public engine::layer
{
public:

	/// <summary>
	/// Default constructor
	/// </summary>
	main_game_layer();
	~main_game_layer();

	/// <summary>
	/// Executes every frame
	/// </summary>
	/// <param name="time_step"></param>
	void on_update(const engine::timestep& time_step) override;

	/// <summary>
	/// Renders the elements on the screen
	/// </summary>
	void on_render() override;

	/// <summary>
	/// Event delegate 
	/// </summary>
	/// <param name="event"></param>
	void on_event(engine::event& event) override;

private:

	// ------------ CAMERAS ------------

	/// <summary>
	/// 2D camera
	/// </summary>
	engine::orthographic_camera m_2d_camera;

	/// <summary>
	/// 3D aamera
	/// </summary>
	engine::perspective_camera m_3d_camera;


	// ---------- ENVIRONMENT --------------

	/// <summary>
	/// (NOT USED CURRENTLY)
	/// </summary>
	void check_bounce();

	/// <summary>
	/// Skybox reference
	/// </summary>
	engine::ref<engine::skybox>	m_skybox{};

	/// <summary>
	/// Terrain reference
	/// </summary>
	engine::ref<engine::game_object> m_terrain{};

	/// <summary>
	/// medical box object pickup handler(Environment object)
	/// </summary>
	engine::ref<pickup> m_pickup{};

	/// <summary>
	/// Gameobjects handler
	/// </summary>
	std::vector<engine::ref<engine::game_object>> m_game_objects{};


	// ------------------------- LIGHT ------------------

	/// <summary>
	/// Directional Light Reference
	/// </summary>
	engine::DirectionalLight m_directionalLight;

	// -------------------- MANAGERS ----------------

	/// <summary>
	/// Physic manager object
	/// </summary>
	engine::ref<engine::bullet_manager> m_physics_manager{};

	/// <summary>
	/// Audio manager object
	/// </summary>
	engine::ref<engine::audio_manager>  m_audio_manager{};

	/// <summary>
	/// Caching previous velocity of sphere
	/// </summary>
	float m_prev_sphere_y_vel = 0.f;

	/// <summary>
	/// Texture manager reference
	/// </summary>
	engine::ref<engine::text_manager>	m_text_manager{};


	// --------------  CHARACTER ------------

	/// <summary>
	/// Mannequin (Animated mesh based object) as player
	/// </summary>
	engine::ref<engine::game_object>	m_mannequin{};
	
	engine::ref<engine::material>		m_material{};

	/// <summary>
	/// Mannequin material
	/// </summary>
	engine::ref<engine::material>		m_mannequin_material{};

	/// <summary>
	/// Player object 
	/// </summary>
	player m_player{};

	/// <summary>
	/// Zombie object (ENEMY)
	/// </summary>
	engine::ref<engine::game_object> m_zombie{};

	/// <summary>
	/// Mannequin material
	/// </summary>
	engine::ref<engine::material>		m_zombie_material{};

	/// <summary>
	/// Zombie handler 
	/// </summary>
	zombie_handler m_zombie_handlerRef{};

	/// <summary>
	/// Did Zombie and player?
	/// </summary>
	bool m_isZombieHit{ false };

	/// <summary>
	/// Did zombie first waves happen?
	/// </summary>
	bool m_isZombieWave1{ false };

	/// <summary>
	/// Did zombie second waves happen?
	/// </summary>
	bool m_isZombieWave2{ false };

	/// <summary>
	/// Did zombie thrid waves happen?
	/// </summary>
	bool m_isZombieWave3{ false };

	// ------------------------- MESH BASED ------------------

	/// <summary>
	/// 3D object cow reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object>	m_cow{};

	/// <summary>
	/// 3D object fence reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object>	m_fence{};

	/// <summary>
	/// 3D object car reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object>	m_car{};

	/// <summary>
	/// 3D object barrel reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object>	m_barrel{};

	/// <summary>
	/// 3D object tree reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object>	m_tree{};

	/// <summary>
	/// 3D object ball reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object>	m_ball{};

	/// <summary>
	/// 3D object jeep reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object> m_jeepBlend;

	/// <summary>
	/// 3D object Ak reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object> m_gun1{};

	/// <summary>
	/// 3D object gun2 reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object> m_gun2{};

	/// <summary>
	/// 3D object SafeHouse reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object> m_safe_house{};

	/// <summary>
	/// 3D object alarm signal reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object> m_alarm_signal{};

	/// <summary>
	/// 3D object shrine reference (Environment object)
	/// </summary>
	engine::ref<engine::game_object> m_shrine1{};
	engine::ref<engine::game_object> m_shrine2{};
	
	// --------------- PRIMITIVES ----------------

	/// <summary>
	/// Object for primitive object house (brown texture)
	/// </summary>
	engine::ref<engine::game_object> m_house_brown{};

	/// <summary>
	/// Object for primitive object house (grey texture)
	/// </summary>
	engine::ref<engine::game_object> m_house_grey{};

	/// <summary>
	/// Object for primitive object safe house gate
	/// </summary>
	engine::ref<engine::game_object> m_safe_house_gate{};

	/// <summary>
	/// Object for primitive object rescur alam
	/// </summary>
	engine::ref<engine::game_object> m_rescue_alarm{};

	/// <summary>
	/// House object material
	/// </summary>
	engine::ref<engine::material> m_house_material{};

	// -------------------- CROSSHAIR -----------------

	/// <summary>
	/// 2D Textures for overlay crosshair
	/// </summary>
	engine::ref<engine::texture_2d> m_texCrosshair{};

	/// <summary>
	/// Quad to render background
	/// </summary>
	engine::ref<quad> m_quad;

	// ------------------------ BALLISTA (GUN) ----------------

	/// <summary>
	/// Bllista object
	/// </summary>
	ballistic m_ballistic;
	engine::ref<engine::material>		m_ballistic_material;

	// ----------------------- HUD -----------------------

	/// <summary>
	/// Game HUD Object
	/// </summary>
	game_hud m_game_hud;

	// ----------------------- SHRINE -----------------------

	/// <summary>
	/// SHRINE Object
	/// </summary>
	shrine m_shrine_ref1;

	/// <summary>
	/// SHRINE Object
	/// </summary>
	shrine m_shrine_ref2;

	// ----------------------- RESCUE ALARM HANDER -----------------------

	/// <summary>
	/// rescue_alarm_handler Object
	/// </summary>
	rescue_alarm_handler m_rescue_alarm_handlerRef;

	// ----------------------- SAFE HOUSE HANDLER -----------------------

	/// <summary>
	/// Safe House Object
	/// </summary>
	safe_house_handler m_safe_house_handlerRef;

	// ----------------------- OBJECT ROTATOR HANDLER -----------------------

	/// <summary>
	/// object_rotator_handler reference
	/// </summary>
	object_rotator_handler m_object_rotator_handlerRef{};

	/// <summary>
	/// Object rotator instance for Gun1 object
	/// </summary>
	object_rotator m_orGun1;

	/// <summary>
	/// Object rotator instance for Gun2 object
	/// </summary>
	object_rotator m_orGun2;


	// ----------------------- BOUNDING BOX AND COLLISION -----------------------

	/// <summary>
	/// Collider box instances
	/// </summary>
	engine::bounding_box m_player_box;
	engine::bounding_box m_gun1_box;
	engine::bounding_box m_gun2_box;
	engine::bounding_box m_cow_box;
	engine::bounding_box m_fence_box1;
	engine::bounding_box m_fence_box2;
	engine::bounding_box m_fence_box3;
	engine::bounding_box m_safe_house_gate_box;
	engine::bounding_box m_alarm_box;
	engine::bounding_box m_shrine1_box;
	engine::bounding_box m_shrine2_box;
	engine::bounding_box m_bulletBox;
	engine::bounding_box m_zombie_wave_trigger1;
	engine::bounding_box m_zombie_wave_trigger2;
	engine::bounding_box m_zombie_wave_trigger3;
	engine::bounding_box m_car1_box;
	engine::bounding_box m_car2_box; 
	engine::bounding_box m_jeep_box;
	engine::bounding_box m_house_brown_box;
	engine::bounding_box m_house_grey_box;
	engine::bounding_box m_safe_house_box;


	/// <summary>
	/// Render collider box?
	/// </summary>
	bool m_isCollidersVisible = true;

	// ------------ CONSTANTS --------------

	/// <summary>
	/// Gun 1 Id
	/// </summary>
	const std::string GUN1_ID = "Gun 1";

	/// <summary>
	/// Audio IDs
	/// </summary>
	const std::string AMBIENT_WIND_BG = "wind_bg";
	const std::string BROKEN_RADIO_BG = "radio_bg";
	const std::string GATE_OPEN = "gate_open";
	const std::string ZOMBIE_HIT = "zombie_hit";
	const std::string PLAYER_WALKING = "player_walking";
	const std::string PLAYER_RUNNING = "player_running";
	const std::string ALARM_SOUND = "alarm_sound";

	// ----------------- LIGHT ---------------------

	/// <summary>
	/// Point lioght object
	/// </summary>
	engine::PointLight m_pointLight;

	/// <summary>
	/// Number of point lights
	/// </summary>
	uint32_t num_point_lights = 1;

	/// <summary>
	/// Light sources instance
	/// </summary>
	engine::ref<engine::material> m_lightsource_material_spot_light_shrine;
	engine::ref<engine::material> m_lightsource_material_spot_light_rescue_alarm;
	engine::ref<engine::material> m_lightsource_material_zombie_wave;

	/// <summary>
	/// Spot light object
	/// </summary>
	engine::SpotLight m_spotLight;

	/// <summary>
	/// Number of Spot lights
	/// </summary>
	uint32_t num_spot_lights = 3;

	// --------------------- END GAME ------------

	/// <summary>
	/// Is game over? (Player heath drop to 0)
	/// </summary>
	bool l_isGameOver = false;

	/// <summary>
	/// Has player reached safe house?
	/// </summary>
	bool l_hasPlayerReachedTheSafeHouse = false;


	// --------------------- TESTING ------------

	/// <summary>
	/// Is testing? (Uses free motion camera only used for debugging environment)
	/// </summary>
	bool m_isTesting = false;

	/// <summary>
	/// Has radio audio played already?
	/// </summary>
	bool m_isRadioAudioPlayed = false;

	// -------------- HELPER FUNCTION --------------

	/// <summary>
	/// Return a tranform object with provided position, rotation and scale
	/// </summary>
	/// <param name="a_tranform"></param>
	/// <param name="a_rotationAxis"></param>
	/// <param name="a_rotationAmount"></param>
	/// <param name="a_scale"></param>
	/// <returns></returns>
	glm::mat4 getTransformedCoordinates(glm::vec3 a_tranform, glm::vec3 a_rotationAxis, float a_rotationAmount, glm::vec3 a_scale);

	/// <summary>
	///  Adds the texture to the quad at the desired tranform and renders in 2D camera
	/// </summary>
	/// <param name="a_mesh_shader_2d"></param>
	/// <param name="a_position"></param>
	/// <param name="a_texture2d"></param>
	/// <param name="a_quad"></param>
	void Add_UI_Element(const engine::ref<engine::shader> a_mesh_shader_2d, glm::vec3 a_position, engine::ref<engine::texture_2d> a_texture2d, engine::ref<quad> a_quad);

	/// <summary>
	/// Creates zombie object
	/// </summary>
	/// <param name="a_zumbie_props"></param>
	/// <returns></returns>
	zombie CreateZombie(engine::game_object_properties a_zumbie_props);
};
