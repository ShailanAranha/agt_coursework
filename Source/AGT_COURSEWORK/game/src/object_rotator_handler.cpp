#include "object_rotator_handler.h"


/// <summary>
/// Default constructor
/// </summary>
object_rotator_handler::object_rotator_handler()
{
	m_fltRotationAmount = 0.0f;
	m_fltRotationAmountInRad = 0.0f;
	m_speed = 25;
}
object_rotator_handler::~object_rotator_handler(){}

/// <summary>
/// Initiatize object instance
/// </summary>
/// <param name="object"></param>
void object_rotator_handler::Initialize(){}

/// <summary>
/// Executes once a frame
/// </summary>
/// <param name="time_step"></param>
void object_rotator_handler::on_update(const engine::timestep& time_step)
{
	 m_fltRotationAmount = m_fltRotationAmount + (float)time_step * m_speed;

	 m_fltRotationAmountInRad = (m_fltRotationAmount * 3.14) / 180;

	for (int i = 0; i < m_lstObjRotators.size(); i++)
	{
		if (m_lstObjRotators[i].m_isRotate)
		{
			m_lstObjRotators[i].m_obj->set_rotation_axis(glm::vec3(0,1,0));
			m_lstObjRotators[i].m_obj->set_rotation_amount(m_fltRotationAmountInRad);
		}
	}
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
/// <param name="shader"></param>
void object_rotator_handler::on_render(const engine::ref<engine::shader>& shader, bool a_isFirstPersonShooter)
{

}

/// <summary>
/// Register object of type object_rotator to the handler 
/// </summary>
/// <param name="a_obj"></param>
void object_rotator_handler::Register(object_rotator& a_obj)
{
	m_lstObjRotators.push_back(a_obj);
}

/// <summary>
/// Unregister object of type object_rotator to the handler 
/// </summary>
/// <param name="a_obj"></param>
void object_rotator_handler::Unregister(object_rotator a_obj)
{
	// NOT IMPLEMENTED
	//m_lstObjRotators.pop_back();
}
