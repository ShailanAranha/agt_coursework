//HEADER GUARD
#ifndef END_GAME_LAYER
#define END_GAME_LAYER

#include <engine.h>

#endif


class quad;

class end_game_layer : public engine::layer
{
public:
    /// <summary>
    /// Default constructor
    /// </summary>
    end_game_layer();

    ~end_game_layer();

    /// <summary>
    /// Executes ones a frame
    /// </summary>
    /// <param name="time_step"></param>
    void on_update(const engine::timestep& time_step) override;

    /// <summary>
    /// Renders the elements on the screen
    /// </summary>
    void on_render() override;

    /// <summary>
    /// Event delegate 
    /// </summary>
    /// <param name="event"></param>
    void on_event(engine::event& event) override;


   

    // -------------- HELPER FUNCTION -----------

    /// <summary>
    ///  Adds the texture to the quad at the desired tranform and renders in 2D camera
    /// </summary>
    /// <param name="a_mesh_shader_2d"></param>
    /// <param name="a_position"></param>
    /// <param name="a_texture2d"></param>
    /// <param name="a_quad"></param>
    void Add_UI_Element(const engine::ref<engine::shader> a_mesh_shader_2d, glm::vec3 a_position, engine::ref<engine::texture_2d> a_texture2d, engine::ref<quad> a_quad);


private:

    // ------------CAMERAS-------------

    /// <summary>
    /// 2D camera ref
    /// </summary>
    engine::orthographic_camera m_2d_camera;


    // ---------------ENVIRONMENT--------------

    /// <summary>
    /// Game Objects in the environment
    /// </summary>
    std::vector<engine::ref<engine::game_object>> m_game_objects{};

    /// <summary>
    /// Directional light ref
    /// </summary>
    engine::DirectionalLight m_directionalLight;

    //----------MANAGERS------------

    /// <summary>
    /// Text manager ref
    /// </summary>
    engine::ref<engine::text_manager>	m_text_manager{};

    // ---------TESTING-----------

    /// <summary>
    /// Is Testing? (Enables freen movement camera)
    /// </summary>
    bool m_isTesting = false;

    // ------------OVERLAY ----------

    /// <summary>
    /// 2D Textures for overlay intro screen
    /// </summary>
    engine::ref<engine::texture_2d> m_intro_bg_texture{};
    engine::ref<engine::texture_2d> m_controls_bg_texture{};
    engine::ref<engine::texture_2d> m_loading_bg_texture{};
    //  engine::ref<engine::texture_2d> m_loading_wheel_texture{}; (TODO:: IMPLEMENT LOADING WHEEL (AFTER MILESTONE 1)
    engine::ref<engine::texture_2d> m_play_normal_state{};
    engine::ref<engine::texture_2d> m_play_highlighted_state{};
    engine::ref<engine::texture_2d> m_controls_normal_state{};
    engine::ref<engine::texture_2d> m_controls_highlighted_state{};
    engine::ref<engine::texture_2d> m_quit_normal_state{};
    engine::ref<engine::texture_2d> m_quit_highlighted_state{};

    /// <summary>
    /// Quad to render background
    /// </summary>
    engine::ref<quad> m_quad;

    /// <summary>
    /// Quad to render buttons
    /// </summary>
    engine::ref<quad> m_buttonQuad;

  

};
