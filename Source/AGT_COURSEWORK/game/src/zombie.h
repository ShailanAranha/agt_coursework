//HEADER GUARD
#ifndef ZOMBIE
#define ZOMBIE

#include <engine.h>
#include "bounding_box.h"

#endif



class zombie
{
public:

	/// <summary>
	/// Connstructor
	/// </summary>
	zombie() {}
	zombie(engine::ref<engine::game_object> a_object, engine::bounding_box a_collider)
	{
		m_object = a_object;
		m_collider = a_collider;
	}

	~zombie() {}

	/// <summary>
	/// Zombie Object instance
	/// </summary>
	engine::ref<engine::game_object> m_object;

	/// <summary>
	/// Zombie collider instance
	/// </summary>
	engine::bounding_box m_collider;
//	glm::vec3 enemy_last_pos;


private:

};
