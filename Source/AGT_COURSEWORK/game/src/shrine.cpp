#include "shrine.h"
#include <engine/mouse_buttons_codes.h>
#include <engine/events/key_event.h>
#include <string>

/// <summary>
/// Default contructor
/// </summary>
shrine::shrine(){}
shrine::~shrine(){}

/// <summary>
/// Initializes the object
/// </summary>
/// <param name="object"></param>
void shrine::initialise(engine::ref<engine::game_object> object)
{
	m_object = object;
}

/// <summary>
/// Executes ones a frame
/// </summary>
/// <param name="time_step"></param>
void shrine::on_update(const engine::timestep& time_step)
{
	if (!m_isShrineUsed)
	{
		return;
	}

	m_fltTimer += time_step;

	if (m_fltTimer > m_fltTimeForNextShrine)
	{
		m_isShrineUsed = false;
	}
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
/// <param name="shader"></param>
void shrine::on_render(const engine::ref<engine::shader>& shader, glm::mat4 l_shrine_tranform,
	engine::ref<engine::material> a_lightsource_material, engine::SpotLight a_spotLight,
	uint32_t a_num_spot_lights, int l_iLightNum)
{

	if (m_isShrineUsed)
	{
		engine::renderer::submit(shader, l_shrine_tranform, m_object);
	}
	else
	{
		std::dynamic_pointer_cast<engine::gl_shader>(shader)->
			set_uniform("gNumSpotLights", (int)a_num_spot_lights);

		a_spotLight.submit(shader, l_iLightNum);

		std::dynamic_pointer_cast<engine::gl_shader>(shader)->
			set_uniform("lighting_on", false);

		a_lightsource_material->submit(shader);

		engine::renderer::submit(shader, m_object->meshes().at(0), l_shrine_tranform);

		std::dynamic_pointer_cast<engine::gl_shader>(shader)->
			set_uniform("lighting_on", true);
	}
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
/// <param name="shader"></param>
void shrine::on_event(engine::event& event)
{

}

/// <summary>
/// Set discharge properties of shire
/// </summary>
void shrine::shireUsed()
{
	m_isShrineUsed = true;
	m_fltTimer = 0.0f;
}

