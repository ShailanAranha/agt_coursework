//HEADER GUARD
#ifndef OBJECT_ROTATOR_HANDER
#define OBJECT_ROTATOR_HANDER

#include <engine.h>
#include "object_rotator.h"

#endif

class object_rotator;

class object_rotator_handler
{
public:

	/// <summary>
	/// Default constructor
	/// </summary>
	object_rotator_handler();
	~object_rotator_handler();

	/// <summary>
	/// Initiatize object instance
	/// </summary>
	/// <param name="object"></param>
	void Initialize();

	/// <summary>
	/// Executes once a frame
	/// </summary>
	/// <param name="time_step"></param>
	void on_update(const engine::timestep& time_step);

	/// <summary>
	/// Renders the elements on the screen
	/// </summary>
	/// <param name="shader"></param>
	void on_render(const engine::ref<engine::shader>& shader, bool a_isFirstPersonShooter);

	/// <summary>
	/// Register object of type object_rotator to the handler 
	/// </summary>
	/// <param name="a_obj"></param>
	void Register(object_rotator& a_obj);

	/// <summary>
	/// Unregister object of type object_rotator to the handler 
	/// </summary>
	/// <param name="a_obj"></param>
	void Unregister(object_rotator a_obj);

	/// <summary>
	/// Collection of Object rotator objects in the agme
	/// </summary>
	/// <returns></returns>
	std::vector<object_rotator> LstObjRotators() { return m_lstObjRotators; };

private:
	/// <summary>
	/// Collection of Object rotator objects in the agme
	/// </summary>
	std::vector<object_rotator> m_lstObjRotators{};

	/// <summary>
	/// Rotation amount in degrees
	/// </summary>
	float m_fltRotationAmount;

	/// <summary>
	/// Rotation amount in rad
	/// </summary>
	float m_fltRotationAmountInRad;

	/// <summary>
	/// Speed of rotataion
	/// </summary>
	float m_speed;
};
