//HEADER GUARD
#ifndef ZOMBIE_HANDLER
#define ZOMBIE_HANDLER

#include <engine.h>
#include "enemy.h"
#include "zombie.h"
#include "player.h"

#endif

class zombie_handler
{
public:

	/// <summary>
	/// Default contructor
	/// </summary
	zombie_handler();
	~zombie_handler();

	/// <summary>
	/// Initializes the object via list of zombie instance list
	/// </summary>
	/// <param name="object"></param>
	void initialise(std::vector<zombie> a_lst_zombies);

	/// <summary>
	/// Initializes the object via list of enemy instance lsit
	/// </summary>
	/// <param name="object"></param>
	void initialise2(std::vector<enemy> a_lst_enemy);

	/// <summary>
	/// Executes ones a frame
	/// </summary>
	/// <param name="time_step"></param>
	/// <param name="a_player"></param>
	/// <param name="a_playerBox"></param>
	/// <param name="a_playerLastPos"></param>
	/// <param name="a_isZombieWave"></param>
	/// <param name="a_bulletBox"></param>
	/// <param name="a_camera"></param>
	void on_update(const engine::timestep& time_step, player& a_player, engine::bounding_box a_playerBox, glm::vec3 a_playerLastPos, bool& a_isZombieWave, engine::bounding_box& a_bulletBox, engine::perspective_camera& a_camera);

	/// <summary>
	///  Renders the elements on the screen
	/// </summary>
	void on_render(const engine::ref<engine::shader>& shader, bool a_isColliderVisible, engine::ref<engine::material> a_zombie_material);

	/// <summary>
	/// Renders the elements on the screen
	/// </summary>
	void on_event(engine::event& event, glm::vec3 a_player);

	/// <summary>
	/// cache the current position of all the zombies
	/// </summary>
	void cache_zombie_lst_positions();

	/// <summary>
	/// Randomise zombuie spawn positions
	/// </summary>
	void RandomizeZombiePosition(glm::vec3 a_playerPosition);

	/// <summary>
	/// Start zombie wave
	/// </summary>
	void InitiateZombieWave(glm::vec3 a_playerPosition);

	/// <summary>
	/// Is Zombie wave active?
	/// </summary>
	bool IsZombieWaveActive() { return m_isZombieWaveActive; }


private:

	/// <summary>
	/// Object instance
	/// </summary>
	engine::ref<engine::game_object> m_object;

	/// <summary>
	/// Collection of enemys
	/// </summary>
	std::vector<enemy> m_lstEnemy { };

	/// <summary>
	/// Enemy objects
	/// </summary>
	enemy m_enemy1{};
	enemy m_enemy2{};
	enemy m_enemy3{};
	enemy m_enemy4{};
	enemy m_enemy5{};

	/// <summary>
	/// Update counter
	/// </summary>
	float m_timer = 0.0f;

	/// <summary>
	/// Is Zombie wave active?
	/// </summary>
	bool m_isZombieWaveActive = false;
};
