#include "rescue_alarm_handler.h"
#include <engine/mouse_buttons_codes.h>
#include <engine/events/key_event.h>
#include <string>

/// <summary>
/// Default contructor
/// </summary>
rescue_alarm_handler::rescue_alarm_handler() {}
rescue_alarm_handler::~rescue_alarm_handler() {}

/// <summary>
/// Initializes the object
/// </summary>
/// <param name="object"></param>
void rescue_alarm_handler::initialise(engine::ref<engine::game_object> object)
{
	m_object = object;
}

/// <summary>
/// Executes ones a frame
/// </summary>
/// <param name="time_step"></param>
void rescue_alarm_handler::on_update(const engine::timestep& time_step)
{
	if (!m_isrescue_alarm_triggered)
	{
		return;
	}

	m_fltCounter += time_step* m_fltSpeed;

	if (m_fltCounter > 1.0f)
	{
		m_fltCounter = 0.0f;
	}

	m_lightsource_material_spot_light_rescue_alarm->set_transparency(m_fltCounter);
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
/// <param name="shader"></param>
void rescue_alarm_handler::on_render(const engine::ref<engine::shader>& shader, glm::mat4 l_rescue_alarm_handler_tranform,
	engine::ref<engine::material> a_lightsource_material, engine::SpotLight a_spotLight,
	uint32_t a_num_spot_lights, int l_iLightNum)
{
	m_lightsource_material_spot_light_rescue_alarm = a_lightsource_material;


	if (!m_isrescue_alarm_triggered)
	{
		engine::renderer::submit(shader, l_rescue_alarm_handler_tranform, m_object);
	}
	else
	{
		std::dynamic_pointer_cast<engine::gl_shader>(shader)->
			set_uniform("gNumSpotLights", (int)a_num_spot_lights);

		a_spotLight.submit(shader, l_iLightNum);

		std::dynamic_pointer_cast<engine::gl_shader>(shader)->
			set_uniform("lighting_on", false);

		a_lightsource_material->submit(shader);

		engine::renderer::submit(shader, m_object->meshes().at(0), l_rescue_alarm_handler_tranform);

		std::dynamic_pointer_cast<engine::gl_shader>(shader)->
			set_uniform("lighting_on", true);
	}
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
/// <param name="shader"></param>
void rescue_alarm_handler::on_event(engine::event& event)
{

}

/// <summary>
/// Rasie Alarm
/// </summary>
void rescue_alarm_handler::TriggerAlarm()
{
	m_isrescue_alarm_triggered = true;
}

