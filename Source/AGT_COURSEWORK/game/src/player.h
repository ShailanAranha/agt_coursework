#pragma once
#include <engine.h>
#include "glm/gtx/rotate_vector.hpp"

class player
{
public:

	/// <summary>
	/// Default contructor
	/// </summary>
	player();
	~player();

	/// <summary>
	/// Initializes thr player
	/// </summary>
	/// <param name="object"></param>
	void Initialise(engine::ref<engine::game_object> object, engine::ref<engine::audio_manager>& a_audio_manager);

	/// <summary>
	/// Executes ones a frame
	/// </summary>
	/// <param name="time_step"></param>
	void on_update(const engine::timestep& time_step, bool a_isGunPickedUp);

	/// <summary>
	/// Renders the elements on the screen
	/// </summary>
	void on_event(engine::event& event);

	/// <summary>
	/// Get player object instance
	/// </summary>
	/// <returns></returns>
	engine::ref<engine::game_object> object() const { return m_object; }

	/// <summary>
	/// Turns the player at provided angle
	/// </summary>
	/// <param name="angle"></param>
	void turn(float angle);

	/// <summary>
	/// Changes the camera angle to first person 
	/// </summary>
	/// <param name="camera"></param>
	void update_camera_to_third_person(engine::perspective_camera& camera);

	/// <summary>
	/// Changes the camera angle to third person 
	/// </summary>
	/// <param name="camera"></param>
	void update_camera_to_first_person(engine::perspective_camera& camera);

	/// <summary>
	/// Sets player to jump animation
	/// </summary>
	void jump();

	/// <summary>
	/// Sets player to idle animation
	/// </summary>
	void idle();

	/// <summary>
	/// Sets player to walking animation
	/// </summary>
	void walking();

	/// <summary>
	/// Sets player to running animation
	/// </summary>
	void running();

	/// <summary>
	/// Is camera angle set ot first person?
	/// </summary>
	/// <returns></returns>
	bool IsFirstPerson() const { return m_isFirstPerson; }

	/// <summary>
	/// Is camera angle set ot first person shooter?
	/// </summary>
	/// <returns></returns>
	bool IsFirstPersonShooter() const { return m_isFirstPersonShooter; }

	/// <summary>
	/// Has player enterwed the safe house??
	/// </summary>
	bool HasPlayerEnteredSafeHouse() { return m_hasPlayerEnteredSafeHouse; }

	/// <summary>
	/// Player health
	/// </summary>
	int PlayerHealth() { return m_iPlayerHealth; }

	/// <summary>
	/// Sets player health
	/// </summary>
	/// <param name="a_iPlayerHealth"></param>
	void SetHealth(int a_iPlayerHealth);

	/// <summary>
	/// Sets player health
	/// </summary>
	/// <param name="a_iPlayerHealth"></param>
	void EnemyHit();

	/// <summary>
	/// Pushes player backwards in the opposite direction of the player
	/// </summary>
	void PushBack(const engine::perspective_camera& camera);

	/// <summary>
	/// Push back speed counter
	/// </summary>
	float m_fltSpeed = 1;

	/// <summary>
	/// IS puch back completed?
	/// </summary>
	bool kick_done = false;

	private:

		/// <summary>
		/// Walking speed of the player
		/// </summary>
		float m_walkSpeed{ 0.0f };

		/// <summary>
		/// Running speed of the player
		/// </summary>
		float m_runSpeed{ 0.0f };

		/// <summary>
		/// Animation timing
		/// </summary>
		float m_timer;

		/// <summary>
		/// Is player Idle?
		/// </summary>
		bool m_isIdle = false;

		/// <summary>
		/// Is player walking?
		/// </summary>
		bool m_isWalking = false;

		/// <summary>
		/// Is player running?
		/// </summary>
		bool m_isRunning = false;

		/// <summary>
		/// Is player jumping?
		/// </summary>
		bool m_isJumping = false;

		/// <summary>
		/// IS camera at first person?
		/// </summary>
		bool m_isFirstPerson = false;

		/// <summary>
		/// Is camera at first person shooter mode?
		/// </summary>
		bool m_isFirstPersonShooter = false;

		/// <summary>
		/// Has player enterwed the safe house??
		/// </summary>
		bool m_hasPlayerEnteredSafeHouse = false;

		/// <summary>
		/// Player object instance
		/// </summary>
		engine::ref<engine::game_object> m_object;

		/// <summary>
		/// Player health
		/// </summary>
		int m_iPlayerHealth;

		/// <summary>
		/// Audio manager object
		/// </summary>
		engine::ref<engine::audio_manager>  m_audio_manager{};

};
