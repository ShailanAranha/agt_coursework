#pragma once
#include <engine.h>

class ballistic
{
public:

	/// <summary>
	/// Default constructor
	/// </summary>
	ballistic();
	~ballistic();

	/// <summary>
	/// Initiatize object instance
	/// </summary>
	/// <param name="object"></param>
	void initialise(engine::ref<engine::game_object> object);

	/// <summary>
	/// Shoot a bullet
	/// </summary>
	/// <param name="camera"></param>
	/// <param name="speed"></param>
	void fire(const engine::perspective_camera& camera, float speed);

	/// <summary>
	/// Executes once a frame
	/// </summary>
	/// <param name="time_step"></param>
	void on_update(const engine::timestep& time_step);

	/// <summary>
	/// Renders the elements on the screen
	/// </summary>
	/// <param name="shader"></param>
	void on_render(const engine::ref<engine::shader>& shader);

	/// <summary>
	///  Event delegate 
	/// </summary>
	/// <returns></returns>
	engine::ref<engine::game_object> object() const { return m_object; }
private:

	/// <summary>
	/// Object instance
	/// </summary>
	engine::ref<engine::game_object> m_object;

	/// <summary>
	/// Speed of the bullet
	/// </summary>
	float m_speed = 0.0f;
};
