//HEADER GUARD
#ifndef INTRO_GAME_LAYER
#define INTRO_GAME_LAYER

#include <engine.h>

#endif


class quad;
enum class EIntroButtons;
enum class EWindow;

class intro_game_layer : public engine::layer
{
public:
    /// <summary>
    /// Default constructor
    /// </summary>
    intro_game_layer();

    ~intro_game_layer();

	/// <summary>
    /// Executes once a frame
    /// </summary>
    /// <param name="time_step"></param>
	void on_update(const engine::timestep& time_step) override;

	/// <summary>
    /// Renders the elements on the screen
    /// </summary>
	void on_render() override;

	/// <summary>
    /// Event delegate 
    /// </summary>
    /// <param name="event"></param>
	void on_event(engine::event& event) override;


    // ---------- GET DATA --------------

    /// <summary>
    /// Get current active intro button 
    /// </summary>
    /// <returns></returns>
    EIntroButtons getCurrentActiveButton() { return m_currentActiveButton; }

    /// <summary>
    /// Get current active window of intro screen
    /// </summary>
    /// <returns></returns>
    EWindow getCurrentActiveWindow() { return m_currentOpenWindow; }

    // -------------- HELPER FUNCTION -----------

    /// <summary>
    ///  Adds the texture to the quad at the desired tranform and renders in 2D camera
    /// </summary>
    /// <param name="a_mesh_shader_2d"></param>
    /// <param name="a_position"></param>
    /// <param name="a_texture2d"></param>
    /// <param name="a_quad"></param>
    void Add_UI_Element(const engine::ref<engine::shader> a_mesh_shader_2d, glm::vec3 a_position, engine::ref<engine::texture_2d> a_texture2d, engine::ref<quad> a_quad);

    /// <summary>
    /// Opens Main intro screen
    /// </summary>
    /// <param name="a_mesh_shader_2d"></param>
    void OpenMainWindow(engine::ref<engine::shader> a_mesh_shader_2d);

    /// <summary>
    /// Opens controls window of intro screen
    /// </summary>
    /// <param name="a_mesh_shader_2d"></param>
    void OpenControlsWindow(engine::ref<engine::shader> a_mesh_shader_2d);

    /// <summary>
    /// Opens loading window of intro screen
    /// </summary>
    /// <param name="a_mesh_shader_2d"></param>
    void OpenLoadingWindow(engine::ref<engine::shader> a_mesh_shader_2d);

    //  ----------------ACTIVATE WINDOW---------

    /// <summary>
    /// Activates main intro screen
    /// </summary>
    void ActivateMainWindow();

    /// <summary>
    /// Activates control window scren
    /// </summary>
    void ActivateControlWindow();

    /// <summary>
    /// Activates control window scren
    /// </summary>
    void ActivateLoadingWindow();

private:

    // ------------CAMERAS-------------

    /// <summary>
    /// 2D camera ref
    /// </summary>
    engine::orthographic_camera m_2d_camera;


    // ---------------ENVIRONMENT--------------

    /// <summary>
    /// Game Objects in the environment
    /// </summary>
    std::vector<engine::ref<engine::game_object>> m_game_objects{};

    /// <summary>
    /// Directional light ref
    /// </summary>
    engine::DirectionalLight m_directionalLight;

    //----------MANAGERS------------

    /// <summary>
    /// Text manager ref
    /// </summary>
    engine::ref<engine::text_manager>	m_text_manager{};

    /// <summary>
    /// Audio manager object
    /// </summary>
    engine::ref<engine::audio_manager>  m_audio_manager{};

    // ---------TESTING-----------

    /// <summary>
    /// Is Testing? (Enables freen movement camera)
    /// </summary>
    bool m_isTesting = false;

    // ------------OVERLAY ----------

    /// <summary>
    /// 2D Textures for overlay intro screen
    /// </summary>
    engine::ref<engine::texture_2d> m_intro_bg_texture{};
    engine::ref<engine::texture_2d> m_controls_bg_texture{};
    engine::ref<engine::texture_2d> m_loading_bg_texture{};
    engine::ref<engine::texture_2d> m_play_normal_state{};
    engine::ref<engine::texture_2d> m_play_highlighted_state{};
    engine::ref<engine::texture_2d> m_controls_normal_state{};
    engine::ref<engine::texture_2d> m_controls_highlighted_state{};
    engine::ref<engine::texture_2d> m_quit_normal_state{};
    engine::ref<engine::texture_2d> m_quit_highlighted_state{};

    /// <summary>
    /// Quad to render background
    /// </summary>
    engine::ref<quad> m_quad;

    /// <summary>
    /// Quad to render buttons
    /// </summary>
    engine::ref<quad> m_buttonQuad;

    /// <summary>
    /// Current active window in the intro scren
    /// </summary>
    EIntroButtons m_currentActiveButton;

    /// <summary>
    /// Current open window in the intro screen
    /// </summary>
    EWindow m_currentOpenWindow;

    // --------------------------- CONSTANTS -----------------------

    /// <summary>
    /// Audio IDs
    /// </summary>
    const std::string BUTTON_CLICK_ID = "button_click";
    const std::string BUTTON_SELECTED_ID = "button_selected";
    const std::string INTRO_BG = "into_bg";

};


/// <summary>
/// Types of interactive buttons in intro main screen
/// </summary>
enum class EIntroButtons
{
    Play = 0,
    Controls = 1,
    Quit = 2
};

/// <summary>
/// Types of screens in Intro
/// </summary>
enum class EWindow
{
    Main = 0,
    Controls = 1,
    Loading = 2
};



