//HEADER GUARD
#ifndef SAFE_HOUSE_HANDLER
#define SAFE_HOUSE_HANDLER

#include <engine.h>

#endif


class safe_house_handler
{
public:

    /// <summary>
    /// Default contructor
    /// </summary>
    safe_house_handler();
    ~safe_house_handler();

    /// <summary>
    /// Initializes the object
    /// </summary>
    /// <param name="object"></param>
    void initialise(engine::ref<engine::game_object> a_object, float a_fltOpenGatePosition = -2.5f, float a_fltCloseGatePosition = 0.5f);

    /// <summary>
    /// Executes ones a frame
    /// </summary>
    /// <param name="time_step"></param>
    void on_update(const engine::timestep& time_step);

    /// <summary>
    /// Renders the elements on the screen
    /// </summary>
    /// <param name="shader"></param>
    void on_render(const engine::ref<engine::shader>& shader, bool a_isFirstPersonShooter);

    /// <summary>
    /// Renders the elements on the screen
    /// </summary>
    /// <param name="shader"></param>
    void on_event(engine::event& event);

    /// <summary>
    /// Open safe house gate
    /// </summary>
    void OpenSafeHouseGate();

    /// <summary>
    /// Close safe house gate
    /// </summary>
    void CloseSafeHouseGate();

    /// <summary>
    /// Is safe house gate open?
    /// </summary>
    bool IsGateOpen() { return m_isGateOpen; }

private:

    /// <summary>
    /// Object instance
    /// </summary>
    engine::ref<engine::game_object> m_object;

    /// <summary>
    /// Y position of safe house when open
    /// </summary>
    float m_fltOpenGatePosition = -2.5f;

    /// <summary>
    /// Y position of safe house when closed
    /// </summary>
    float m_fltCloseGatePosition = 0.5f;

    /// <summary>
    /// Movement speed
    /// </summary>
    float m_fltSpeed = 1.0f;

    /// <summary>
    /// MOvement magnitude
    /// </summary>
    float m_fltMovement = 0.5f;

    /// <summary>
    /// Is safe house gate open?
    /// </summary>
    bool m_isGateOpen = false;

};



