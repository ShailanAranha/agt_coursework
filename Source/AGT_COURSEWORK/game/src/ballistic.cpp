#include "ballistic.h"
#include "pch.h"

/// <summary>
/// Default constructor
/// </summary>
ballistic::ballistic(){}

ballistic::~ballistic(){}

/// <summary>
/// Initiatize object instance
/// </summary>
/// <param name="object"></param>
void ballistic::initialise(engine::ref<engine::game_object> object)
{
	// CREATE AN OBJECT POOL HERE
	m_object = object;
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
/// <param name="shader"></param>
void ballistic::on_render(const engine::ref<engine::shader>& shader)
{
	engine::renderer::submit(shader, m_object);
}

/// <summary>
/// Executes once a frame
/// </summary>
/// <param name="time_step"></param>
void ballistic::on_update(const engine::timestep& time_step)
{
	m_object->set_position(m_object->position() + m_object->forward() * (float)time_step * m_speed);
}

/// <summary>
/// Shoot a bullet
/// </summary>
/// <param name="camera"></param>
/// <param name="speed"></param>
void ballistic::fire(const engine::perspective_camera& camera, float speed)
{
	m_object->set_position(camera.position());
	m_object->set_forward(camera.front_vector());
	m_speed = speed;
}

