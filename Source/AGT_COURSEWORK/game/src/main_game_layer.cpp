#include "main_game_layer.h"
#include "platform/opengl/gl_shader.h"

#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp>
#include <engine/mouse_buttons_codes.h>
#include <engine/events/key_event.h>
#include "engine/utils/track.h"

#include "quad.h"

/// <summary>
/// Default constructor
/// </summary>
main_game_layer::main_game_layer()
	:m_2d_camera(-1.6f, 1.6f, -0.9f, 0.9f),
	m_3d_camera((float)engine::application::window().width(), (float)engine::application::window().height(), 45.f, 0.1f, 250.f)
{
	// Hide the mouse and lock it inside the window
	//engine::input::anchor_mouse(true);
	engine::application::window().hide_mouse_cursor();


	// ---------------- INITIAZIE AUDIO MANAGER AND LOAD AUDIOS (TODO:: AFTER MILESTONE 1) ---------

	// Initialise audio and play background music
	m_audio_manager = engine::audio_manager::instance();
	m_audio_manager->init();

	m_audio_manager->load_sound("assets/audio/ambient_wind_bg.mp3", engine::sound_type::track, AMBIENT_WIND_BG); // Royalty free sound from freesound.org
	m_audio_manager->load_sound("assets/audio/radio_bg.mp3", engine::sound_type::spatialised, BROKEN_RADIO_BG); // Royalty free sound from freesound.org
	m_audio_manager->load_sound("assets/audio/Gate_sound.mp3", engine::sound_type::track, GATE_OPEN); // Royalty free sound from freesound.org
	m_audio_manager->load_sound("assets/audio/enemy_hit.mp3", engine::sound_type::track, ZOMBIE_HIT); // Royalty free sound from freesound.org
	m_audio_manager->load_sound("assets/audio/alarm_sound.mp3", engine::sound_type::track, ALARM_SOUND); // Royalty free sound from freesound.org
	m_audio_manager->load_sound("assets/audio/walking.mp3", engine::sound_type::track, PLAYER_WALKING); // Royalty free sound from freesound.org
	m_audio_manager->load_sound("assets/audio/running.mp3", engine::sound_type::track, PLAYER_RUNNING); // Royalty free sound from freesound.org

	//m_audio_manager->loop(BROKEN_RADIO_BG, true);
	//m_audio_manager->volume(BROKEN_RADIO_BG, 1);
	m_audio_manager->loop(ZOMBIE_HIT, false);
	m_audio_manager->loop(GATE_OPEN, false);
	m_audio_manager->loop(AMBIENT_WIND_BG, true);
	m_audio_manager->play(AMBIENT_WIND_BG);

	// ----------------SHADER LIBRARY AND LIGHT INFORMATION -------------------

	// Initialise the shaders, materials and lights
	auto mesh_shader = engine::renderer::shaders_library()->get("mesh");
	auto text_shader = engine::renderer::shaders_library()->get("text_2D");

	m_directionalLight.Color = glm::vec3(1.0f, 1.0f, 1.0f);
	//m_directionalLight.AmbientIntensity = 0.25f;
	m_directionalLight.AmbientIntensity = 0.25f;
	m_directionalLight.DiffuseIntensity = 0.6f;
	m_directionalLight.Direction = glm::normalize(glm::vec3(1.0f, -1.0f, 0.0f));

	// POINT LIGHT
	m_pointLight.Color = glm::vec3(1.0f, .0f, .0f);
	m_pointLight.AmbientIntensity = 1.f;
	m_pointLight.DiffuseIntensity = 1.f;
	m_pointLight.Position = glm::vec3(0.0f, 2.0f, 50.0f);
	 

	m_spotLight.Color = glm::vec3(.0f, .0f, 1.0f);
	m_spotLight.AmbientIntensity = 0.25f;
	m_spotLight.DiffuseIntensity = 0.6f;
	m_spotLight.Position = glm::vec3(0.f, 1.5f, 80.f);
	m_spotLight.Direction = glm::normalize(glm::vec3(1.0f, -1.0f, 0.0f));
	m_spotLight.Cutoff = 0.5f;


	// set color texture unit
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->bind();
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("lighting_on", true);
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("gColorMap", 0);
	m_directionalLight.submit(mesh_shader);
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("gMatSpecularIntensity", 1.f);
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("gSpecularPower", 10.f);
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("transparency", 1.0f);

	std::dynamic_pointer_cast<engine::gl_shader>(text_shader)->bind();
	std::dynamic_pointer_cast<engine::gl_shader>(text_shader)->set_uniform("projection",
		glm::ortho(0.f, (float)engine::application::window().width(), 0.f,
			(float)engine::application::window().height()));

	// ------------------ MATERIAL ------------------


	m_material = engine::material::create(1.0f, glm::vec3(1.0f, 0.1f, 0.07f),
		glm::vec3(1.0f, 0.1f, 0.07f), glm::vec3(0.5f, 0.5f, 0.5f), 1.0f);

	m_mannequin_material = engine::material::create(1.0f, glm::vec3(0.5f, 0.5f, 0.5f),
		glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(0.5f, 0.5f, 0.5f), 1.0f);


	m_lightsource_material_spot_light_shrine = engine::material::create(1.0f, m_spotLight.Color, m_spotLight.Color, m_spotLight.Color, 0.8f);
	m_lightsource_material_spot_light_rescue_alarm = engine::material::create(1.0f, glm::vec3(1.0f, .0f, .0f), glm::vec3(1.0f, .0f, .0f), glm::vec3(1.0f, .0f, .0f), 1.0f);
	m_lightsource_material_zombie_wave = engine::material::create(1.0f, m_pointLight.Color, m_pointLight.Color, m_pointLight.Color, 1.0f);
	
	// ------------------ SKYBOX ------------------
	
	// ASSET REFERENCE:: Skybox texture from http://www.vwall.it/wp-content/plugins/canvasio3dpro/inc/resource/cubeMaps/
	m_skybox = engine::skybox::create(150.f,
		{
		  engine::texture_2d::create("assets/textures/skybox/sea/front.bmp", true),
		  engine::texture_2d::create("assets/textures/skybox/sea/right.bmp", true),
		  engine::texture_2d::create("assets/textures/skybox/sea/back.bmp", true),
		  engine::texture_2d::create("assets/textures/skybox/sea/left.bmp", true),
		  engine::texture_2d::create("assets/textures/skybox/sea/top.bmp", true),
		  engine::texture_2d::create("assets/textures/skybox/sea/bot.bmp", true)
		});


	// ------------------------------ TERRAIN -----------------------------

	// Load the terrain texture and create a terrain mesh. Create a terrain object. Set its properties

	// ASSET REFERENCE:: TERRAIN texture from https://pxhere.com/en/photo/1026388
	std::vector<engine::ref<engine::texture_2d>> terrain_textures = { engine::texture_2d::create("assets/textures/Terrain/terrain.jpg", false) };
	engine::ref<engine::terrain> terrain_shape = engine::terrain::create(80.f, 0.5f, 80.f);
	engine::game_object_properties terrain_props;
	terrain_props.meshes = { terrain_shape->mesh() };
	terrain_props.textures = terrain_textures;
	terrain_props.position = { 0.f, 0.0f, 40.f };
	terrain_props.is_static = true;
	terrain_props.type = 0;
	terrain_props.bounding_shape = glm::vec3(100.f, 0.5f, 100.f);
	terrain_props.restitution = 0.92f;
	m_terrain = engine::game_object::create(terrain_props);

	// ----------------MANNEQUIN AS PLAYER, TODO:: CHANGE WITH A MESH BASED CHARACTER AFTER MILESTONE 1 -------------

	engine::ref<engine::skinned_mesh> m_skinned_mesh = engine::skinned_mesh::create("assets/models/animated/mannequin/free3Dmodel.dae");
	m_skinned_mesh->LoadAnimationFile("assets/models/animated/mannequin/idle.dae");
	m_skinned_mesh->LoadAnimationFile("assets/models/animated/mannequin/walking.dae");
	m_skinned_mesh->LoadAnimationFile("assets/models/animated/mannequin/jump.dae");
	m_skinned_mesh->LoadAnimationFile("assets/models/animated/mannequin/standard_run.dae");
	m_skinned_mesh->switch_root_movement(false);

	engine::game_object_properties mannequin_props;
	mannequin_props.animated_mesh = m_skinned_mesh;
	mannequin_props.scale = glm::vec3(1.f / glm::max(m_skinned_mesh->size().x, glm::max(m_skinned_mesh->size().y, m_skinned_mesh->size().z)));
	mannequin_props.position = glm::vec3(3.0f, 0.5f, -5.0f);
	mannequin_props.type = 0;
	///mannequin_props.bounding_shape = m_skinned_mesh->size() / 2.f * mannequin_props.scale.x;
	mannequin_props.bounding_shape = glm::vec3(m_skinned_mesh->size().x / 2.f, m_skinned_mesh->size().y, m_skinned_mesh->size().x / 2.f);
	m_mannequin = engine::game_object::create(mannequin_props);


	m_player_box.set_box(mannequin_props.bounding_shape.x * mannequin_props.scale.x,
		mannequin_props.bounding_shape.y * mannequin_props.scale.x,
		mannequin_props.bounding_shape.z * mannequin_props.scale.x,
		mannequin_props.position);

	m_player.Initialise(m_mannequin, m_audio_manager); // ASSIGN MANEQUIN AS PLAYER

	m_skinned_mesh->switch_animation(1);


	// -------------------------- ZUMBIE (ENEMY) ------------------------

	// CREATE ZUMBIE
	//engine::ref<engine::skinned_mesh> m_skinned_mesh_zombie = engine::skinned_mesh::create("assets/models/animated/zombie/zombiemix.dae");
	//std::vector<engine::ref<engine::texture_2d>> temp_textures2 = { engine::texture_2d::create("assets/models/animated/zombie/file2.png", false) };
	//m_skinned_mesh_zombie->LoadAnimationFile("assets/models/animated/zombie/Zombie Idle.dae");

	//mixamo
	//engine::ref<engine::skinned_mesh> m_skinned_mesh_zombie = engine::skinned_mesh::create("assets/models/animated/zombie/zombiemix.dae");
	//std::vector<engine::ref<engine::texture_2d>> temp_textures2 = { engine::texture_2d::create("assets/models/animated/zombie/zombie_girl/textures/zombie_diffuse.png", false) };
	//m_skinned_mesh_zombie->LoadAnimationFile("assets/models/animated/zombie/zombie_girl/zombiegirl_idle.dae");

	/*engine::ref<engine::skinned_mesh> m_skinned_mesh_zombie = engine::skinned_mesh::create("assets/models/animated/zombie/zombie_final/zombie_final.dae");
	std::vector<engine::ref<engine::texture_2d>> temp_textures2 = { engine::texture_2d::create("assets/models/animated/zombie/zombie_final/textures/zombie_final.png", false) };
	m_skinned_mesh_zombie->LoadAnimationFile("assets/models/animated/zombie/zombie_final/zombie_final_idle.dae");*/


	m_zombie_material = engine::material::create(1.0f, glm::vec3(1.0f, 0.5f, 0.07f),
		glm::vec3(1.0f, 0.1f, 0.1f), glm::vec3(0.5f, 0.5f, 0.5f), 1.0f);


	engine::ref<engine::skinned_mesh> m_skinned_mesh_zombie = engine::skinned_mesh::create("assets/models/animated/zombie/free3DmodelZombie.dae");
	std::vector<engine::ref<engine::texture_2d>> temp_textures2 = { engine::texture_2d::create("assets/models/animated/zombie/zombie_final/textures/zombie_final.png", false) };
	m_skinned_mesh_zombie->LoadAnimationFile("assets/models/animated/mannequin/idle.dae");

	m_skinned_mesh_zombie->switch_root_movement(false);

	m_skinned_mesh_zombie->switch_animation(1);


	engine::game_object_properties zumbie_props;
	zumbie_props.animated_mesh = m_skinned_mesh_zombie;
	//zumbie_props.textures = temp_textures2;
	zumbie_props.scale = glm::vec3(1.f / glm::max(m_skinned_mesh_zombie->size().x, glm::max(m_skinned_mesh_zombie->size().y, m_skinned_mesh_zombie->size().z)));
	//zumbie_props.position = glm::vec3(4.f, 0.5f, -5.f);
	zumbie_props.position = glm::vec3(0.f, 0.5f, 85.0f);
	zumbie_props.type = 0; 
	zumbie_props.bounding_shape = m_skinned_mesh_zombie->size() / 2.f * zumbie_props.scale.x;
	m_zombie = engine::game_object::create(zumbie_props);

	std::vector<zombie> m_lst_zombies;

	zombie m_zombie1{ CreateZombie(zumbie_props) };
	zombie m_zombie2{ CreateZombie(zumbie_props) };
	zombie m_zombie3{ CreateZombie(zumbie_props) };
	zombie m_zombie4{ CreateZombie(zumbie_props) };
	zombie m_zombie5{ CreateZombie(zumbie_props) };

	m_lst_zombies.push_back(m_zombie1);
	m_lst_zombies.push_back(m_zombie2);
	m_lst_zombies.push_back(m_zombie3);
	m_lst_zombies.push_back(m_zombie4);
	m_lst_zombies.push_back(m_zombie5);

	m_zombie_handlerRef.initialise(m_lst_zombies);


	// --------------- MESH BASED 3D OBJECTS ------------------

	// INITIALIZE COW OBJECT AND PROPERTIES
	engine::ref <engine::model> cow_model = engine::model::create("assets/models/static/cow4.3ds");
	std::vector<engine::ref<engine::texture_2d>> temp_textures3 = { engine::texture_2d::create("assets/models/static/fence/fence.jpg", false) };

	engine::game_object_properties cow_props;
	cow_props.meshes = cow_model->meshes();
	cow_props.textures = cow_model->textures();
	float cow_scale = 1.f / glm::max(cow_model->size().x, glm::max(cow_model->size().y,cow_model->size().z));
	cow_props.position = { 7.f, 0.5, 90.f };
	cow_props.rotation_axis = glm::vec3(0, 1, 0);
	cow_props.rotation_amount = 3.14 / 3;
	cow_props.scale = glm::vec3(cow_scale);
	cow_props.bounding_shape = cow_model->size();
	m_cow = engine::game_object::create(cow_props);

	m_cow->set_offset(cow_model->offset());

	m_cow_box.set_box(cow_props.bounding_shape.x* cow_scale*2.2f, cow_props.bounding_shape.y
		* cow_scale, cow_props.bounding_shape.z* cow_scale/2, cow_props.position);


	// INITIALIZE SHRINE OBJECT AND PROPERTIES
	//  ASSET REFERENCE:: https://www.cgtrader.com/items/2645645/download-page
	engine::ref <engine::model> shrine_model = engine::model::create("assets/models/static/shrine/shrine.fbx");
	std::vector<engine::ref<engine::texture_2d>> shrine_tex = { engine::texture_2d::create("assets/models/static/shrine/shrine.jpg", false) };

	engine::game_object_properties shrine_props;
	shrine_props.meshes = shrine_model->meshes();
	shrine_props.textures = shrine_tex;
	float shrine_scale = 1.f / glm::max(shrine_model->size().x, glm::max(shrine_model->size().y, shrine_model->size().z));
	shrine_props.position = { -10.f, 1.5f, 70.f };
	shrine_props.rotation_axis = glm::vec3(1,0,0);
	shrine_props.rotation_amount = -3.14/2;
	shrine_props.scale = glm::vec3(shrine_scale* 1.5f);
	shrine_props.bounding_shape = shrine_model->size();

	m_shrine1 = engine::game_object::create(shrine_props);
	m_shrine2 = engine::game_object::create(shrine_props);
	

	m_shrine1->set_offset(shrine_model->offset());
	m_shrine1_box.set_box(0.25f, 2.0f, 0.25f, shrine_props.position - glm::vec3(0.f, 1.5f, -0.25f));


	m_shrine2->set_offset(shrine_model->offset());
	m_shrine2_box.set_box(0.25f, 2.0f, 0.25f, glm::vec3(10.f, 1.5f, 50.f) - glm::vec3(0.f, 1.5f, -0.25f));

	m_shrine_ref1.initialise(m_shrine1);
	m_shrine_ref2.initialise(m_shrine2);

	// INITIALIZE ALARAM OBJECT AND PROPERTIES
	//  ASSET REFERENCE:: https://www.cgtrader.com/items/312535/download-page
	engine::ref <engine::model> alarm_signal_model = engine::model::create("assets/models/static/alarm_signal/alarm_signal.fbx");

	engine::game_object_properties alarm_props;
	alarm_props.meshes = alarm_signal_model->meshes();
	alarm_props.textures = alarm_signal_model->textures();
	float alarm_scale = 1.f / glm::max(alarm_signal_model->size().x, glm::max(alarm_signal_model->size().y, alarm_signal_model->size().z));
	alarm_props.position = { -0.15f +19.f , 1.3f, 90.3f - 89.0f };
	alarm_props.rotation_axis = glm::vec3(1, 1, 0);
	alarm_props.rotation_amount = 3.14/1.15;
	alarm_props.scale = glm::vec3(alarm_scale/4);
	alarm_props.bounding_shape = alarm_signal_model->size();
	m_alarm_signal = engine::game_object::create(alarm_props);

	m_rescue_alarm_handlerRef.initialise(m_alarm_signal);

	// INITIALIZE SAFE HOUSE OBJECT AND PROPERTIES

	
	//  ASSET REFERENCE:: SAFE HOUSE https://www.turbosquid.com/3d-models/3d-lighthouse-light-1609811
	engine::ref <engine::model> safe_house_model = engine::model::create("assets/models/static/safe_house/safe_house.fbx");

	engine::game_object_properties safe_house_props;
	safe_house_props.meshes = safe_house_model->meshes();
	safe_house_props.textures = safe_house_model->textures();
	float safe_house_scale = 1.f / glm::max(safe_house_model->size().x, glm::max(safe_house_model->size().y, safe_house_model->size().z));
	safe_house_props.position = { 0.f, 0.5, -20.f };
	safe_house_props.rotation_axis = glm::vec3(0, 1, 0);
	safe_house_props.rotation_amount = 0;
	safe_house_props.scale = glm::vec3(safe_house_scale * 10.0f);
	safe_house_props.bounding_shape = safe_house_model->size();
	m_safe_house = engine::game_object::create(safe_house_props);

	m_safe_house->set_offset(safe_house_model->offset());
	m_safe_house_box.set_box(safe_house_props.bounding_shape.x* safe_house_scale* 10, safe_house_props.bounding_shape.y
		* safe_house_scale * 10, safe_house_props.bounding_shape.z* safe_house_scale * 10, safe_house_props.position);

	// INITIALIZE FENCE OBJECT AND PROPERTIES
	// ASSET REFERENCE:: Fence: https://www.turbosquid.com/3d-models/free-obj-model-railing/426248
	engine::ref <engine::model> fence_model = engine::model::create("assets/models/static/fence/fence.fbx");
	std::vector<engine::ref<engine::texture_2d>> fence_texture = { engine::texture_2d::create("assets/models/static/fence/fence.jpg", false) };

	engine::game_object_properties fence_props;
	fence_props.meshes = fence_model->meshes();
	fence_props.textures = fence_texture;
	float fence_scale = 1.f / glm::max(fence_model->size().x, glm::max(fence_model->size().y, fence_model->size().z));
	fence_props.position = { 20.f, 0.5, 99.5f };
	fence_props.rotation_axis = glm::vec3(0, 1, 0);
	fence_props.rotation_amount = 0;
	fence_props.scale = glm::vec3(fence_scale);
	fence_props.bounding_shape = fence_model->size();
	m_fence = engine::game_object::create(fence_props);

	m_fence->set_offset(fence_model->offset());

	m_fence_box1.set_box(fence_props.bounding_shape.x* fence_scale , fence_props.bounding_shape.y
		* fence_scale, fence_props.bounding_shape.z* fence_scale * 100.0f, glm::vec3(20.5f, 0.5, 50.0f));

	m_fence_box2.set_box(fence_props.bounding_shape.x* fence_scale, fence_props.bounding_shape.y
		* fence_scale, fence_props.bounding_shape.z* fence_scale * 100.0f, glm::vec3(-20.5f, 0.5, 50.0f));

	m_fence_box3.set_box(fence_props.bounding_shape.x* fence_scale * 600, fence_props.bounding_shape.y
		* fence_scale, fence_props.bounding_shape.z* fence_scale/2 , glm::vec3(0.f, 0.5, 100.0f));

	// INITIALIZE CAR OBJECT AND PROPERTIES
	// ASSET REFERENCE:: Car: https://www.cgtrader.com/free-3d-models/car/standard/renault-12tl-1971
	engine::ref <engine::model> car_model = engine::model::create("assets/models/static/car_model.dae");
	std::vector<engine::ref<engine::texture_2d>> car_model_tex = { engine::texture_2d::create("assets/models/static/car_model.png", false) };

	engine::game_object_properties car_props;
	car_props.meshes = car_model->meshes();
	car_props.textures = car_model_tex;
	float car_scale = 2.5f / glm::max(car_model->size().x, glm::max(car_model->size().y, car_model->size().z));
	car_props.position = { 5.f, 0.5, 90.f };
	car_props.rotation_amount = -3.14/2;
	car_props.rotation_axis = glm::vec3(1,0,0);
	car_props.scale = glm::vec3(car_scale);
	car_props.bounding_shape = car_model->size() / 2.f * car_scale;
	m_car = engine::game_object::create(car_props);

	m_car->set_offset(car_model->offset());

	m_car1_box.set_box(car_props.bounding_shape.x* car_scale *5, car_props.bounding_shape.y
		* car_scale, car_props.bounding_shape.z*13* car_scale, glm::vec3(7.f, 0.6f, 85.f));

	m_car2_box.set_box(car_props.bounding_shape.x* car_scale * 5, car_props.bounding_shape.y
		* car_scale, car_props.bounding_shape.z * 13 * car_scale, glm::vec3(0.f, 0.5f, 40.f));

	// INITIALIZE BROKEN BARREL OBJECT AND PROPERTIES
	// ASSET REFERENCE:: Broken Barrel: https://www.cgtrader.com/free-3d-models/watercraft/industrial/broken-barrel
	engine::ref <engine::model> barrel_model = engine::model::create("assets/models/static/broken_barrel.fbx");
	engine::game_object_properties barrel_props;
	barrel_props.meshes = barrel_model->meshes();
	barrel_props.textures = barrel_model->textures();
	float barrel_scale = 1.f / glm::max(barrel_model->size().x, glm::max(barrel_model->size().y, barrel_model->size().z));
	barrel_props.position = { 5.f, 0.5, 90.f };
	barrel_props.scale = glm::vec3(barrel_scale);
	barrel_props.bounding_shape = barrel_model->size() / 2.f * barrel_scale;
	m_barrel = engine::game_object::create(barrel_props);

	// INITIALIZE TREE OBJECT AND PROPERTIES
	engine::ref <engine::model> tree_model = engine::model::create("assets/models/static/elm.3ds");
	engine::game_object_properties tree_props;
	tree_props.meshes = tree_model->meshes();
	tree_props.textures = tree_model->textures();
	float tree_scale = 3.f / glm::max(tree_model->size().x, glm::max(tree_model->size().y, tree_model->size().z));
	tree_props.position = { 4.f, 0.5f, -5.f };
	tree_props.bounding_shape = tree_model->size() / 2.f * tree_scale;
	tree_props.scale = glm::vec3(tree_scale);
	m_tree = engine::game_object::create(tree_props);

	// INITIALIZE SPHERE OBJECT AND PROPERTIES
	engine::ref<engine::sphere> sphere_shape = engine::sphere::create(10, 20, 0.5f);
	engine::game_object_properties sphere_props;
	sphere_props.position = { 0.f, 7.f, -5.f };
	sphere_props.meshes = { sphere_shape->mesh() };
	sphere_props.type = 1;
	sphere_props.bounding_shape = glm::vec3(0.5f);
	sphere_props.restitution = 0.92f;
	sphere_props.mass = 0.000001f;

	m_ball = engine::game_object::create(sphere_props);


	// INITIALIZE JEEP OBJECT AND PROPERTIES
	engine::ref <engine::model> blend_jeep_model = engine::model::create("assets/models/static/jeep.obj");
	engine::game_object_properties blend_jeep_props;
	blend_jeep_props.meshes = blend_jeep_model->meshes();
	blend_jeep_props.textures = blend_jeep_model->textures();
	//float jeep_scale = 1.f / glm::max(jeep_model->size().x, glm::max(jeep_model->size().y, jeep_model->size().z));
	float blend_jeep_scale = 1.0f;
	blend_jeep_props.position = { -10.0f,0.5f, 0.0f };
	blend_jeep_props.scale = glm::vec3(blend_jeep_scale);
	blend_jeep_props.bounding_shape = blend_jeep_model->size() / 2.f * blend_jeep_scale;
	m_jeepBlend = engine::game_object::create(blend_jeep_props); 

	m_jeepBlend->set_offset(blend_jeep_model->offset());

	m_jeep_box.set_box(blend_jeep_props.bounding_shape.x* blend_jeep_scale *2, blend_jeep_props.bounding_shape.y
		* blend_jeep_scale * 2, blend_jeep_props.bounding_shape.z * blend_jeep_scale * 2, glm::vec3(-7.f, 0.5f, 85.0f));

	// INITIALIZE GUN 1 OBJECT AND PROPERTIES

	engine::ref <engine::model> gun_1_model = engine::model::create("assets/models/static/gun/gun1.fbx");

	engine::game_object_properties gun1_props;
	gun1_props.meshes = gun_1_model->meshes();
	gun1_props.textures = gun_1_model->textures();
	float gun1_scale = 1.f / glm::max(gun_1_model->size().x, glm::max(gun_1_model->size().y, gun_1_model->size().z));
	gun1_props.position = { 0.f, 0.6f, 92.f };
	gun1_props.scale = glm::vec3(gun1_scale);
//	gun1_props.bounding_shape = gun_1_model->size() / 2.f * gun1_scale;
	gun1_props.bounding_shape = gun_1_model->size();
	m_gun1 = engine::game_object::create(gun1_props);

	engine::ref <engine::model> gun_2_model = engine::model::create("assets/models/static/gun/gun2.fbx");


	m_gun1->set_offset(gun_1_model->offset());
	m_gun1_box.set_box(gun1_props.bounding_shape.x* gun1_scale, gun1_props.bounding_shape.y
		* gun1_scale, gun1_props.bounding_shape.z* gun1_scale, gun1_props.position);


	engine::game_object_properties gun2_props;
	gun2_props.meshes = gun_2_model->meshes();
	gun2_props.textures = gun_2_model->textures();
	float gun2_scale = 1.f / glm::max(gun_2_model->size().x, glm::max(gun_2_model->size().y, gun_2_model->size().z));
	gun2_props.position = { 2.f, 0.5, 60.f };
	gun2_props.scale = glm::vec3(gun2_scale);
	//gun2_props.bounding_shape = gun_2_model->size() / 2.f * gun2_scale;
	gun2_props.bounding_shape = gun_2_model->size();
	m_gun2 = engine::game_object::create(gun2_props);

	m_gun2->set_offset(gun_2_model->offset());
	m_gun2_box.set_box(gun2_props.bounding_shape.x* gun2_scale, gun2_props.bounding_shape.y
		* gun2_scale, gun2_props.bounding_shape.z* gun2_scale, gun2_props.position);

	//object_rotator m_orGun1;
	m_orGun1 = { m_gun1 , GUN1_ID, true};
	m_object_rotator_handlerRef.Register(m_orGun1);

	m_orGun2 = { m_gun2 , "Gun 2", true };
	m_object_rotator_handlerRef.Register(m_orGun2);

	// --------------- PRIMITIVE GAMEOBJECT --------------
	std::vector<glm::vec3> house_vertices;

	house_vertices.push_back(glm::vec3(0.f, 4.f, 0.f)); //0
	house_vertices.push_back(glm::vec3(-2.f, 2.f, 2.f)); //1
	house_vertices.push_back(glm::vec3(2.f, 2.f, 2.f)); //2
	house_vertices.push_back(glm::vec3(2.f, 2.f, -2.f)); //3
	house_vertices.push_back(glm::vec3(-2.f, 2.f, -2.f)); //4
	house_vertices.push_back(glm::vec3(-2.f, 0.f, 2.f)); //5
	house_vertices.push_back(glm::vec3(2.f, 0.f, 2.f)); //6
	house_vertices.push_back(glm::vec3(2.f, 0.f, -2.f)); //7
	house_vertices.push_back(glm::vec3(-2.f, 0.f, -2.f)); //8

	engine::ref<engine::house> house_shape = engine::house::create(house_vertices);
	engine::game_object_properties house_props;
	house_props.position = { 0.f, 0.5f, -10.f };
	house_props.meshes = { house_shape->mesh() };

	// ASSET REFERENCE:: Grey wood texture: https://www.piqsels.com/en/public-domain-photo-fkrvx
	// ASSET REFERENCE:: Brown wood texture: https://www.piqsels.com/en/public-domain-photo-oeoos/download
	std::vector<engine::ref<engine::texture_2d>> house_textures = { engine::texture_2d::create("assets/textures/primitives/wood.jpg", false) };
	std::vector<engine::ref<engine::texture_2d>> house_texture_wood_grey = { engine::texture_2d::create("assets/textures/primitives/wood_grey.jpg", false) };

	house_props.textures = house_textures;
	m_house_brown = engine::game_object::create(house_props);

	house_props.textures = house_texture_wood_grey;
	m_house_grey = engine::game_object::create(house_props);

	m_house_brown_box.set_box(4.0f, 4.f, 4.f, glm::vec3(10.f, 0.5f, 75.f));
	m_house_grey_box.set_box(4.0f, 4.f, 4.f, glm::vec3(-15.f, 0.5f, 30.f));

	// ------------ SAFE HOUSE PRIMITIVE ---------

	std::vector<glm::vec3> safe_house_gate_vertices;

	safe_house_gate_vertices.push_back(glm::vec3(0.f, 2.f, 0.f)); //0
	safe_house_gate_vertices.push_back(glm::vec3(-0.5f, 0.5f, 0.5f)); //1
	safe_house_gate_vertices.push_back(glm::vec3(0.5f, 0.5f, 0.5f)); //2
	safe_house_gate_vertices.push_back(glm::vec3(0.5f, 0.5f, -0.5f)); //3
	safe_house_gate_vertices.push_back(glm::vec3(-0.5f, 0.5f, -0.5f)); //4
	safe_house_gate_vertices.push_back(glm::vec3(-0.5f, 0.f, 0.5f)); //5
	safe_house_gate_vertices.push_back(glm::vec3(0.5f, 0.f, 0.5f)); //6
	safe_house_gate_vertices.push_back(glm::vec3(0.5f, 0.f, -0.5f)); //7
	safe_house_gate_vertices.push_back(glm::vec3(-0.5f, 0.f, -0.5f)); //8

	engine::ref<engine::safe_house_gate> safe_house_gate_shape = engine::safe_house_gate::create(safe_house_gate_vertices);
	engine::game_object_properties safe_house_gate_props;
	safe_house_gate_props.position = { 0.f, 0.5f, -1.f };
	safe_house_gate_props.meshes = { safe_house_gate_shape->mesh() };

	// ASSET REFERENCE:: Metal texture: https://pixabay.com/illustrations/metal-silver-stainless-steel-316803/
	std::vector<engine::ref<engine::texture_2d>> safe_house_texture = { engine::texture_2d::create("assets/textures/primitives/metal.jpg", false) };

	safe_house_gate_props.textures = safe_house_texture;
	m_safe_house_gate = engine::game_object::create(safe_house_gate_props);


	m_safe_house_gate_box.set_box(40.0f, 2.0f , 2.0f, safe_house_gate_props.position);


	// --------------- RESCUR ALA--------------
	std::vector<glm::vec3> rescue_alarm_vertices;

	rescue_alarm_vertices.push_back(glm::vec3(-0.1f, 0.5f, 0.1f)); //0
	rescue_alarm_vertices.push_back(glm::vec3(0.1f, 0.5f, 0.1f)); //1
	rescue_alarm_vertices.push_back(glm::vec3(0.1f, 0.5f, -0.1f)); //2
	rescue_alarm_vertices.push_back(glm::vec3(-0.1f, 0.5f, -0.1f)); //3
	rescue_alarm_vertices.push_back(glm::vec3(-0.1f, 0.f, 0.1f)); //4
	rescue_alarm_vertices.push_back(glm::vec3(0.1f, 0.f, 0.1f)); //5
	rescue_alarm_vertices.push_back(glm::vec3(0.1f, 0.f, -0.1f)); //6
	rescue_alarm_vertices.push_back(glm::vec3(-0.1f, 0.f, -0.1f)); //7

	rescue_alarm_vertices.push_back(glm::vec3(-0.5f, 0.5f, 0.5f)); //8
	rescue_alarm_vertices.push_back(glm::vec3(0.5f, 0.5f, 0.5f)); //9
	rescue_alarm_vertices.push_back(glm::vec3(0.5f, 1.f, 0.f)); //10
	rescue_alarm_vertices.push_back(glm::vec3(-0.5f, 0.5f, -0.5f)); //11
	rescue_alarm_vertices.push_back(glm::vec3(0.5f, 0.5f, -0.5f)); //12
	rescue_alarm_vertices.push_back(glm::vec3(-0.5f, 1.f, 0.f)); //13


	engine::ref<engine::rescue_alarm> rescue_alarm_shape = engine::rescue_alarm::create(rescue_alarm_vertices);
	engine::game_object_properties rescue_alarm_props;
	rescue_alarm_props.position = { 0.f +19.0f, 0.5f, 90.f -89.0f };
	rescue_alarm_props.meshes = { rescue_alarm_shape->mesh() };

	std::vector<engine::ref<engine::texture_2d>> rescue_alarm_textures = { engine::texture_2d::create("assets/textures/primitives/metal.jpg", false) };

	rescue_alarm_props.textures = rescue_alarm_textures;
	m_rescue_alarm = engine::game_object::create(rescue_alarm_props);

	m_alarm_box.set_box(1.0f, 1.0f, 1.0f, rescue_alarm_props.position);

	m_game_objects.push_back(m_terrain);
	m_game_objects.push_back(m_ball);


	// ---------- INITIALIZE MANAGERS -----------
	m_physics_manager = engine::bullet_manager::create(m_game_objects);
	m_text_manager = engine::text_manager::create();

	// ---------------------- CROSSHAIR -------------------------
	m_texCrosshair = engine::texture_2d::create("assets/gui/Crosshair/green_crosshair.png", true);
	m_quad = quad::create(glm::vec2(0.1f, 0.1f));


	// ------------------------- BALLISTA (GUN) ----------------------

	//m_ballistic_material = engine::material::create(1.f, glm::vec3(1.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 0.0f), glm::vec3(1.0f, 1.0f, 0.0f), 1.0f);
	m_ballistic_material = engine::material::create(1.f, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), 1.0f);

	engine::ref<engine::sphere> sphere_shape_bullets = engine::sphere::create(20, 40, 0.1f);
	engine::game_object_properties sphere_props_bullets;
	sphere_props_bullets.position = { -2.5f, 0.0f, -2.5f };
	sphere_props_bullets.meshes = { sphere_shape_bullets->mesh() };
	sphere_props_bullets.type = 1;
	sphere_props_bullets.bounding_shape = glm::vec3(0.1f);
	sphere_props_bullets.restitution = 0.92f;
	sphere_props_bullets.mass = 0.000001f;

	m_ballistic.initialise(engine::game_object::create(sphere_props_bullets));


	m_bulletBox.set_box(sphere_props_bullets.bounding_shape.x* 1, sphere_props_bullets.bounding_shape.y
		* 1, sphere_props_bullets.bounding_shape.z* 1, sphere_props_bullets.position);

	// --------------- Zombie wave -----------------

	m_zombie_wave_trigger1.set_box(40.0f, 2.0f, 2.f, glm::vec3(0.f, 0.5f, 80.f));
	m_zombie_wave_trigger2.set_box(40.0f, 2.0f, 2.f, glm::vec3(0.f, 0.5f, 50.f));
	m_zombie_wave_trigger3.set_box(40.0f, 2.0f, 2.f, glm::vec3(0.f, 0.5f, 30.f));


	// ---------------------- GAME HUD ----------------------
	m_game_hud.initialise(m_text_manager);


	// ---------------------- SAFE HOUSE HANDLER ----------------------
	m_safe_house_handlerRef.initialise(m_safe_house_gate);

}

main_game_layer::~main_game_layer() {}

/// <summary>
/// Executes every frame
/// </summary>
/// <param name="time_step"></param>
void main_game_layer::on_update(const engine::timestep& time_step)
{
	// CACHING
	m_zombie_handlerRef.cache_zombie_lst_positions();
	glm::vec3 l_lastPosition = m_player.object()->position();
	
	// ---------------  CAMERA UPDATE ---------------
	if (m_player.IsFirstPersonShooter())
	{
		m_3d_camera.on_update_to_FPS(time_step);
	}
	else
	{
		m_3d_camera.on_update(time_step);
	}

	// ---------------- MANAGER UPDATE ---------------
	m_physics_manager->dynamics_world_update(m_game_objects, double(time_step));
	m_audio_manager->update_with_camera(m_3d_camera);

	// ------------------- EMENY UPDATE --------------------

	//m_mannequin->animated_mesh()->on_update(time_step);
	//m_zombie->animated_mesh()->on_update(time_step);

	// ------------- PLAYER UPDATE -------------------
	m_player.on_update(time_step, m_game_hud.m_gun1.m_isGunPickedUp || m_game_hud.m_gun2.m_isGunPickedUp);

	if (m_player.HasPlayerEnteredSafeHouse())
	{
		m_safe_house_handlerRef.CloseSafeHouseGate();
		m_game_hud.ChangeStatus(EPlayerStatus::EnteredTheSafeHouse);
		m_audio_manager->pause(ALARM_SOUND);
		m_audio_manager->play(GATE_OPEN);
	}

	// ---------------------- SAFE HOUSE HANDLER ----------------------
	m_safe_house_handlerRef.on_update(time_step);


	// ----------------- CAMERA ANGLE -------------



	if (m_isTesting)
	{
		m_3d_camera.camera_free_movement(time_step);
	}
	else
	{
		if (!m_player.IsFirstPersonShooter())
		{
			if (m_player.IsFirstPerson())
			{
				m_player.update_camera_to_first_person(m_3d_camera);
			}
			else
			{
				m_player.update_camera_to_third_person(m_3d_camera);
			}
		}

	}

	// ------------------------- BALLISTA (GUN) ----------------------
	m_ballistic.on_update(time_step);
	m_bulletBox.on_update(m_ballistic.object()->position());

	if (m_player.IsFirstPersonShooter())
	{
		if (engine::input::mouse_button_pressed_once(engine::mouse_button_codes::MOUSE_BUTTON_LEFT))
		{
			m_ballistic.fire(m_3d_camera, 30.0f);
		}

		// CALLED TO TRIGGER THE RELEASE EVENT
		if (engine::input::mouse_button_released(engine::mouse_button_codes::MOUSE_BUTTON_LEFT))
		{

		}
	}


	// ---------------------- GAME HUD ----------------------

	float l_conversionValue = 100.0f / 95.0f;
	float l_playerZPos = m_player.object()->position().z;
	float l_value = l_playerZPos * l_conversionValue;

	m_game_hud.updateDistanceFromSafeHouse(l_value);
	m_game_hud.on_update(time_step);

	// ---------------------- OBJECT ROTATOR HANDLER ----------------------
	m_object_rotator_handlerRef.on_update(time_step);

	// ---------------------- SHRINE  ----------------------
	m_shrine_ref1.on_update(time_step);
	m_shrine_ref2.on_update(time_step);

	// ---------------------- RESCUE ALARM  ----------------------
	m_rescue_alarm_handlerRef.on_update(time_step);

	// ----------------------- BOUNDING BOX AND COLLISION -----------------------

	m_player_box.on_update(m_player.object()->position());

	m_zombie_handlerRef.on_update(time_step, m_player , m_player_box, l_lastPosition, m_isZombieHit, m_bulletBox, m_3d_camera);

	if (m_cow_box.collision(m_player_box)|| m_player_box.collision(m_fence_box1) || m_player_box.collision(m_fence_box2) || m_player_box.collision(m_fence_box3)
		|| m_player_box.collision(m_car1_box) || m_player_box.collision(m_car2_box) || m_player_box.collision(m_jeep_box)
		|| m_player_box.collision(m_house_brown_box) || m_player_box.collision(m_house_grey_box))
	{
		m_player.object()->set_position(l_lastPosition);
	}

	if (m_car1_box.collision(m_player_box)&& !m_isRadioAudioPlayed)
	{
		m_isRadioAudioPlayed = true;
		m_audio_manager->play_spatialised_sound(BROKEN_RADIO_BG, m_3d_camera.position(), glm::vec3(7.f, 0.6f, 85.f));
	}

	if (m_player_box.collision(m_gun1_box) && !m_game_hud.m_gun1.m_isGunPickedUp)
	{
		m_game_hud.m_gun1.m_isGunPickedUp = m_game_hud.m_gun1.m_isGunSelected = true;
		m_game_hud.m_gun2.m_isGunSelected = false;

		m_orGun1.m_isRotate = false;
		/*for (int i = 0; i < m_object_rotator_handlerRef.LstObjRotators().size(); i++)
		{
			if (m_object_rotator_handlerRef.LstObjRotators()[i].Id() == GUN1_ID)
			{
				m_object_rotator_handlerRef.LstObjRotators()[i].m_isRotate = false;
			}
		}*/
	}

	if (m_player_box.collision(m_gun2_box) && !m_game_hud.m_gun2.m_isGunPickedUp)
	{
		m_game_hud.m_gun2.m_isGunPickedUp  = m_game_hud.m_gun2.m_isGunSelected = true;
		m_game_hud.m_gun1.m_isGunSelected = false;

		m_orGun2.m_isRotate = false;
	//	m_orGun1.m_isRotate = false;

	}

	if (m_player_box.collision(m_safe_house_gate_box) && !m_safe_house_handlerRef.IsGateOpen())
	{
		m_player.object()->set_position(l_lastPosition);
	}

	if (m_player_box.collision(m_alarm_box))
	{
		m_player.object()->set_position(l_lastPosition);
		m_game_hud.ChangeStatus(EPlayerStatus::AtTheRescueAlarm);
	}

	if (m_player_box.collision(m_shrine1_box))
	{
		m_player.object()->set_position(l_lastPosition);

			if (!m_shrine_ref1.IsShrineUsed())
			{
				m_shrine_ref1.shireUsed();
				m_player.SetHealth(100);
			}
	}

	if (m_player_box.collision(m_shrine2_box))
	{
		m_player.object()->set_position(l_lastPosition);

		if (!m_shrine_ref2.IsShrineUsed())
		{
			m_shrine_ref2.shireUsed();
			m_player.SetHealth(100);
		}
	}

	if (m_player_box.collision(m_zombie_wave_trigger1) && !m_isZombieWave1)
	{
		m_isZombieWave1 = true;
		m_zombie_handlerRef.InitiateZombieWave(m_player.object()->position());
		m_game_hud.ZombieWaveDisplayMessage = "ZOMBIE WAVE 1";
	}

	if (m_player_box.collision(m_zombie_wave_trigger2) && !m_isZombieWave2)
	{
		m_isZombieWave2 = true;
		m_zombie_handlerRef.InitiateZombieWave(m_player.object()->position());
		m_game_hud.ZombieWaveDisplayMessage = "ZOMBIE WAVE 2";
	}

	if (m_player_box.collision(m_zombie_wave_trigger3) && !m_isZombieWave3)
	{
		m_isZombieWave3 = true;
		m_zombie_handlerRef.InitiateZombieWave(m_player.object()->position());
		m_game_hud.ZombieWaveDisplayMessage = "ZOMBIE WAVE 3";
	}

	// ------------------------- END GAME ------------------
	if (m_player.PlayerHealth() <= 0)
	{
		l_isGameOver = true;
	}

	if (m_player_box.collision(m_safe_house_box))
	{
		l_isGameOver = l_hasPlayerReachedTheSafeHouse =  true;
	}
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
void main_game_layer::on_render()
{
	engine::render_command::clear_color({ 0.2f, 0.3f, 0.3f, 1.0f });
	engine::render_command::clear();

#pragma region END GAME SCREEN

	if (l_isGameOver)
	{
		m_audio_manager->stop_all();
		if (l_hasPlayerReachedTheSafeHouse)
		{
			const auto text_shader = engine::renderer::shaders_library()->get("text_2D");
			m_text_manager->render_text(text_shader, "CONGRATULATIONS!! YOU HAVE REACHED THE SAFE HOUSE!!", 350.0f, 400.0f, 0.4f, glm::vec4(1.f, 0.5f, 0.f, 1.f));
		}
		else
		{
			const auto text_shader = engine::renderer::shaders_library()->get("text_2D");
			m_text_manager->render_text(text_shader, "GAME OVER!!", 500.0f, 320.0f, 1.f, glm::vec4(1.f, 0.5f, 0.f, 1.f));
		}

		return;
	}

#pragma endregion


#pragma region SKYBOX, TERRAIN, SHADER. AND CAMERA SETUP

	// Set up  shader. (renders textures and materials)
	const auto mesh_shader = engine::renderer::shaders_library()->get("mesh");
	engine::renderer::begin_scene(m_3d_camera, mesh_shader);

	// Set up some of the scene's parameters in the shader
	std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->set_uniform("gEyeWorldPos", m_3d_camera.position());

	// Position the skybox centred on the player and render it
	glm::mat4 skybox_tranform(1.0f);
	skybox_tranform = glm::translate(skybox_tranform, m_3d_camera.position());
	for (const auto& texture : m_skybox->textures())
	{
		texture->bind();
	}

	engine::renderer::submit(mesh_shader, m_skybox, skybox_tranform);
	engine::renderer::submit(mesh_shader, m_terrain);

#pragma endregion

#pragma region ENVIRONMENT SETUP (SPAWNING OBJECTS)

	// -------------------------- PRIMITIVES -----------------------------

	// ------------------------- BROWN HOUSE -------------------
	glm::mat4 house_brown_transform = getTransformedCoordinates(glm::vec3(10.f, 0.5f, 75.f), m_house_brown->rotation_axis(), m_house_brown->rotation_amount(), m_house_brown->scale());
	engine::renderer::submit(mesh_shader, house_brown_transform, m_house_brown);

	// ------------------------ GREY HOUSE -------------------
	glm::mat4 house_grey_transform = getTransformedCoordinates(glm::vec3(-15.f, 0.5f, 30.f), m_house_grey->rotation_axis(), m_house_grey->rotation_amount(), m_house_grey->scale());
	engine::renderer::submit(mesh_shader, house_grey_transform, m_house_grey);

	// ------------------------ SAFE HOUSE GATE -------------------
	//glm::mat4 safe_house_gate_transform = getTransformedCoordinates(m_safe_house_gate->position(), m_safe_house_gate->rotation_axis(), m_safe_house_gate->rotation_amount(), m_safe_house_gate->scale());
	//engine::renderer::submit(mesh_shader, safe_house_gate_transform, m_safe_house_gate);

	
	// ---------------------- SAFE HOUSE HANDLER ----------------------
	m_safe_house_handlerRef.on_render(mesh_shader, m_player.IsFirstPersonShooter());

	// -------------------- MESH BASED -----------------------------


	// --------- SAFE HOUSE -------------
	glm::mat4 l_safe_house_tranform = getTransformedCoordinates(m_safe_house->position(), m_safe_house->rotation_axis(), m_safe_house->rotation_amount(), m_safe_house->scale());
	engine::renderer::submit(mesh_shader, l_safe_house_tranform, m_safe_house);

	// --------- TREES , SPAWN TREES (TREE DISTRIBUTION)---------

	int l_iTotalTreeCount = 11;

	for (int i = 0; i < l_iTotalTreeCount; i++)
	{
		// PAIR 1
		glm::mat4 tree_transform_pair1 = getTransformedCoordinates(glm::vec3(22, 0.5, i * i), m_tree->rotation_axis(), m_tree->rotation_amount(), glm::vec3(0.1 * i, 0.1 * i, 0.1 * i));
		engine::renderer::submit(mesh_shader, tree_transform_pair1, m_tree);

		// PAIR 2
		glm::mat4 tree_transform_pair2 = getTransformedCoordinates(glm::vec3(-22, 0.5, i * i + 20), m_tree->rotation_axis(), m_tree->rotation_amount(), glm::vec3(0.1 * i, 0.1 * i, 0.1 * i));
		engine::renderer::submit(mesh_shader, tree_transform_pair2, m_tree);
	}

	// ------------ JEEP ---------------
	glm::mat4 blend_jeep_transform = getTransformedCoordinates(glm::vec3(-7.f, 0.5f, 85.0f), glm::vec3(0, 1, 0), 3.14 / 4, m_jeepBlend->scale());
	engine::renderer::submit(mesh_shader, blend_jeep_transform, m_jeepBlend);

	// --------- COW -------------
	//glm::mat4 l_cow_tranform = getTransformedCoordinates(glm::vec3(7.f, 0.5, 90.f), glm::vec3(0, 1, 0), 3.14 / 3, m_cow->scale());
	glm::mat4 l_cow_tranform = getTransformedCoordinates(glm::vec3(7.f, 0.5, 90.f) - glm::vec3(m_cow -> offset().x, 0.f, m_cow->offset().z) * m_cow->scale().x, glm::vec3(0, 1, 0), 3.14 / 3, m_cow->scale());
	engine::renderer::submit(mesh_shader, l_cow_tranform, m_cow);

	// --------- CAR -------------
	glm::mat4 l_car1_tranform = getTransformedCoordinates(glm::vec3(7.f, 0.6, 85.f), m_car->rotation_axis(), m_car->rotation_amount(), m_car->scale());
	engine::renderer::submit(mesh_shader, l_car1_tranform, m_car);

	glm::mat4 l_car2_tranform = getTransformedCoordinates(glm::vec3(0.f, 1.2f, 40.f), glm::vec3(1,0,0), 3.14 / 2, m_car->scale());
	engine::renderer::submit(mesh_shader, l_car2_tranform, m_car);

	// --------- BROKEN BARREL -------------

#pragma region BROKEN BARREL

	for (int i = 0; i < 10; i++)
	{
		glm::mat4 l_barrel_tranform = getTransformedCoordinates(glm::vec3(0.f, 0.5, 90.f -i*i ), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);

		 l_barrel_tranform = getTransformedCoordinates(glm::vec3(3.f, 0.5, 90.f - i * i -3.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);

		l_barrel_tranform = getTransformedCoordinates(glm::vec3(-3.f, 0.5, 90.f - i * i - 3.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);
	
		 l_barrel_tranform = getTransformedCoordinates(glm::vec3(5.f, 0.5, 90.f - i * i - 1.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);

		l_barrel_tranform = getTransformedCoordinates(glm::vec3(-5.f, 0.5, 90.f - i * i - 1.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);
	
		 l_barrel_tranform = getTransformedCoordinates(glm::vec3(7.f, 0.5, 90.f - i * i - 2.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);

		l_barrel_tranform = getTransformedCoordinates(glm::vec3(-7.f, 0.5, 90.f - i * i - 2.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);

		 l_barrel_tranform = getTransformedCoordinates(glm::vec3(9.f, 0.5, 90.f - i * i - 5.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);

		l_barrel_tranform = getTransformedCoordinates(glm::vec3(-9.f, 0.5, 90.f - i * i - 5.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);
	
		 l_barrel_tranform = getTransformedCoordinates(glm::vec3(13.f, 0.5, 90.f - i * i - 9.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);

		 l_barrel_tranform = getTransformedCoordinates(glm::vec3(-13.f, 0.5, 90.f - i * i - 9.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);

		 l_barrel_tranform = getTransformedCoordinates(glm::vec3(17.f, 0.5, 90.f - i * i - 3.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);

		l_barrel_tranform = getTransformedCoordinates(glm::vec3(-17.f, 0.5, 90.f - i * i - 3.f), m_barrel->rotation_axis(), m_barrel->rotation_amount(), m_barrel->scale());
		engine::renderer::submit(mesh_shader, l_barrel_tranform, m_barrel);
	}
	

#pragma endregion

	// ------------- GUN 1 -----------------

	if (!m_game_hud.m_gun1.m_isGunPickedUp)
	{
		glm::mat4 l_gun1_tranform = getTransformedCoordinates(glm::vec3(0.f, 0.6f, 92.f) - glm::vec3(m_gun1-> offset().x, 0.f, m_gun1->offset().z) * m_gun1->scale().x, m_gun1->rotation_axis(), m_gun1->rotation_amount(), m_gun1->scale());
		engine::renderer::submit(mesh_shader, l_gun1_tranform, m_gun1);
	}

	if (!m_game_hud.m_gun2.m_isGunPickedUp)
	{
		//glm::mat4 l_gun2_tranform = getTransformedCoordinates(glm::vec3(2.f, 0.5, 90.f), m_gun2->rotation_axis(), m_gun2->rotation_amount(), m_gun2->scale());
		glm::mat4 l_gun2_tranform = getTransformedCoordinates(glm::vec3(2.f, 0.5, 60.f) - glm::vec3(m_gun2->offset().x, 0.f, m_gun2->offset().z) * m_gun2->scale().x, m_gun2->rotation_axis(), m_gun2->rotation_amount(), m_gun2->scale());
		engine::renderer::submit(mesh_shader, l_gun2_tranform, m_gun2);
	}

	// --------- FENCE -------------

	int l_iFenceCount = 100;


	for (int i = 0; i < l_iFenceCount; i++)
	{
		// PAIR 1
		glm::mat4 l_fence_tranform_p1 = getTransformedCoordinates(glm::vec3( 20.5f, 0.5f, 99.5f - i), m_fence->rotation_axis(), m_fence->rotation_amount(), m_fence->scale());
		engine::renderer::submit(mesh_shader, l_fence_tranform_p1, m_fence);

		// PAIR 2
		glm::mat4 l_fence_tranform_p2 = getTransformedCoordinates(glm::vec3(-20.5f, 0.5f, 99.5f - i), m_fence->rotation_axis(), m_fence->rotation_amount(), m_fence->scale());
		engine::renderer::submit(mesh_shader, l_fence_tranform_p2, m_fence);
	}

	int l_iStartFencCount = 21;

	for (int i = 0; i < l_iStartFencCount; i++)
	{
		// PAIR 1
		glm::mat4 l_fence_tranform_p1 = getTransformedCoordinates(glm::vec3(i, 0.5f, 100.f), glm::vec3(0,1,0), 3.14/2, m_fence->scale());
		engine::renderer::submit(mesh_shader, l_fence_tranform_p1, m_fence);

		// PAIR 2
		glm::mat4 l_fence_tranform_p2 = getTransformedCoordinates(glm::vec3(-i, 0.5f, 100.f), glm::vec3(0, 1, 0),3.14 / 2, m_fence->scale());
		engine::renderer::submit(mesh_shader, l_fence_tranform_p2, m_fence);
	}

#pragma endregion

#pragma region PLAYER AND ENEMEY (ZOMBIES)


	m_zombie_handlerRef.on_render(mesh_shader, m_isCollidersVisible, m_zombie_material);

	if (!m_player.IsFirstPersonShooter())
	{
		m_mannequin_material->submit(mesh_shader);
		engine::renderer::submit(mesh_shader, m_player.object());
	}

	// ----------------------- BOUNDING BOX AND COLLISION -----------------------
	if (m_isCollidersVisible)
	{
		m_player_box.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_cow_box.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_gun1_box.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_gun2_box.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_fence_box1.on_render(2.5f, 0.f, 0.f, mesh_shader); 
		m_fence_box2.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_fence_box3.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_safe_house_gate_box.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_alarm_box.on_render(2.5f, 0.f, 0.f, mesh_shader); 
		m_shrine1_box.on_render(2.5f, 0.f, 0.f, mesh_shader); 
		m_shrine2_box.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_bulletBox.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_zombie_wave_trigger1.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_zombie_wave_trigger2.on_render(2.5f, 0.f, 0.f, mesh_shader); 
		m_zombie_wave_trigger3.on_render(2.5f, 0.f, 0.f, mesh_shader); 
		m_car1_box.on_render(2.5f, 0.f, 0.f, mesh_shader); 
		m_car2_box.on_render(2.5f, 0.f, 0.f, mesh_shader); 
		m_jeep_box.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_house_brown_box.on_render(2.5f, 0.f, 0.f, mesh_shader); 
		m_house_grey_box.on_render(2.5f, 0.f, 0.f, mesh_shader);
		m_safe_house_box.on_render(2.5f, 0.f, 0.f, mesh_shader);
	}

#pragma endregion

#pragma region GUNS AND OTHER GAME ELEMENTS
	// ---------------------- BALLISTA -----------------------------
	m_ballistic_material->submit(mesh_shader);
	m_ballistic.on_render(mesh_shader);


	// ---------------------- OBJECT ROTATOR HANDLER ----------------------
	m_object_rotator_handlerRef.on_render(mesh_shader, m_player.IsFirstPersonShooter());


#pragma region LIGHTING

	// --------- SHRINE ------------- (MESH BASED OBJECT)

	// POINT LIGHT

	if (m_isZombieHit)
	{
		m_pointLight.Color = glm::vec3(1.f, 0.f,0.f);
		m_pointLight.AmbientIntensity = 1.0f;
		m_pointLight.DiffuseIntensity = 1.0f;
	}
	else
	{
		m_pointLight.Color = glm::vec3(0.f, 0.f, 0.f);
		m_pointLight.AmbientIntensity = 0.0f;
		m_pointLight.DiffuseIntensity = 0.0f;
	}

		std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->
			set_uniform("gNumPointLights", (int)num_point_lights);
		m_pointLight.submit(mesh_shader, 0);

		std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->
			set_uniform("lighting_on", false);

		m_lightsource_material_zombie_wave->submit(mesh_shader);

		//engine::renderer::submit(mesh_shader, m_ball->meshes().at(0),
			//glm::translate(glm::mat4(1.f), m_pointLight.Position));

		std::dynamic_pointer_cast<engine::gl_shader>(mesh_shader)->
			set_uniform("lighting_on", true);

	
	// -------------- SPOT LIGHT ----------
	glm::mat4 l_shrine_tranform1 = getTransformedCoordinates(m_shrine1->position() - glm::vec3(m_shrine1->offset().x, 0.f, m_shrine1->offset().z) * m_shrine1->scale().x, m_shrine1->rotation_axis(), m_shrine1->rotation_amount(), m_shrine1->scale());

	glm::mat4 l_shrine_tranform2 = getTransformedCoordinates(glm::vec3(10.f, 1.5f, 50.f) - glm::vec3(m_shrine2->offset().x, 0.f, m_shrine2->offset().z) * m_shrine2->scale().x, m_shrine2->rotation_axis(), m_shrine2->rotation_amount(), m_shrine2->scale());

	m_spotLight.Color = glm::vec3(.0f, .0f, 1.0f);
	m_spotLight.Position = glm::vec3(-10.f, 1.5f, 70.f);
	m_shrine_ref1.on_render(mesh_shader, l_shrine_tranform1, m_lightsource_material_spot_light_shrine, m_spotLight, num_spot_lights, 0);

	m_spotLight.Color = glm::vec3(.0f, .0f, 1.0f);
	m_spotLight.Position = glm::vec3(10.f, 1.5f, 50.f);
	m_shrine_ref2.on_render(mesh_shader, l_shrine_tranform2, m_lightsource_material_spot_light_shrine, m_spotLight, num_spot_lights, 1);

	//------------------------ RESCUE ALARM-------------------

	glm::mat4 rescue_alarm_transform = getTransformedCoordinates(m_rescue_alarm->position(), glm::vec3(0, 1, 0), 3.14 * -1.15f, m_rescue_alarm->scale());
	engine::renderer::submit(mesh_shader, rescue_alarm_transform, m_rescue_alarm);

	glm::mat4 alarm_signal_transform = getTransformedCoordinates(m_alarm_signal->position(), m_alarm_signal->rotation_axis(), m_alarm_signal->rotation_amount(), m_alarm_signal->scale());
//	engine::renderer::submit(mesh_shader, alarm_signal_transform, m_alarm_signal);

	//m_spotLight.Color = glm::vec3(1.0f, .0f, .0f); 
	m_spotLight.Position = m_alarm_signal->position();
	m_rescue_alarm_handlerRef.on_render(mesh_shader, alarm_signal_transform, m_lightsource_material_spot_light_rescue_alarm, m_spotLight, num_spot_lights, 3);

#pragma endregion


	engine::renderer::end_scene();
#pragma region PLAYER AND ENEMEY (ZOMBIES)


	const auto mesh_shader_2d = engine::renderer::shaders_library()->get("mesh");
	engine::renderer::begin_scene(m_2d_camera, mesh_shader_2d);
	if (m_player.IsFirstPersonShooter())
	{
		Add_UI_Element(mesh_shader_2d, glm::vec3(0.f, 0.0f, 0.1f), m_texCrosshair, m_quad);
	}


	// ---------------------- GAME HUD ----------------------
	m_game_hud.on_render(mesh_shader_2d, m_player.IsFirstPersonShooter(), m_player.HasPlayerEnteredSafeHouse(), m_player.PlayerHealth(), m_zombie_handlerRef.IsZombieWaveActive());

	engine::renderer::end_scene();

	// Render text
	/*const auto text_shader = engine::renderer::shaders_library()->get("text_2D");
	m_text_manager->render_text(text_shader, "Orange Text", 10.f, (float)engine::application::window().height() - 25.f, 0.5f, glm::vec4(1.f, 0.5f, 0.f, 1.f));*/
}

/// <summary>
/// Event delegate 
/// </summary>
/// <param name="event"></param>
void main_game_layer::on_event(engine::event& event)
{
	if (event.event_type() == engine::event_type_e::key_pressed)
	{
		auto& e = dynamic_cast<engine::key_pressed_event&>(event);
		if (e.key_code() == engine::key_codes::KEY_TAB)
		{
			engine::render_command::toggle_wireframe();
		}

		if (e.key_code() == engine::key_codes::KEY_1)
		{
			m_isCollidersVisible = !m_isCollidersVisible;
		}

		if (e.key_code() == engine::key_codes::KEY_F && (m_game_hud.CurrentPlayerStatus() == EPlayerStatus::AtTheRescueAlarm) )
		{
			m_safe_house_handlerRef.OpenSafeHouseGate();
			m_game_hud.ChangeStatus(EPlayerStatus::TriggeredRescueAlarm);
			m_rescue_alarm_handlerRef.TriggerAlarm();
		//	m_audio_manager->play(GATE_OPEN);
			m_audio_manager->play(ALARM_SOUND);
		}

		// FOR TESTING
		if (e.key_code() == engine::key_codes::KEY_2)
		{
			m_isZombieHit = !m_isZombieHit;
		}
		
	}

	m_player.on_event(event);
	m_game_hud.on_event(event);
	m_safe_house_handlerRef.on_event(event);
	m_shrine_ref1.on_event(event);
	m_shrine_ref2.on_event(event); 
	m_rescue_alarm_handlerRef.on_event(event);
	m_zombie_handlerRef.on_event(event, m_player.object()->position());
}

/// <summary>
/// (NOT USED CURRENTLY)
/// </summary>
void main_game_layer::check_bounce()
{
	if (m_prev_sphere_y_vel < 0.1f && m_ball->velocity().y > 0.1f)
		//m_audio_manager->play("bounce");
		m_audio_manager->play_spatialised_sound("bounce", m_3d_camera.position(), glm::vec3(m_ball->position().x, 0.f, m_ball->position().z));
	m_prev_sphere_y_vel = m_game_objects.at(1)->velocity().y;
}


#pragma region HELPER METHOD

/// <summary>
/// Return a tranform object with provided position, rotation and scale
/// </summary>
/// <param name="a_tranform"></param>
/// <param name="a_rotationAxis"></param>
/// <param name="a_rotationAmount"></param>
/// <param name="a_scale"></param>
/// <returns></returns>
glm::mat4 main_game_layer::getTransformedCoordinates(glm::vec3 a_tranform, glm::vec3 a_rotationAxis, float a_rotationAmount, glm::vec3 a_scale)
{
	glm::mat4 l_transform(1.0f);
	l_transform = glm::translate(l_transform, a_tranform);
	l_transform = glm::rotate(l_transform, a_rotationAmount, a_rotationAxis);
	l_transform = glm::scale(l_transform, a_scale);

	return l_transform;
}

/// <summary>
///  Adds the texture to the quad at the desired tranform and renders in 2D camera
/// </summary>
/// <param name="a_mesh_shader_2d"></param>
/// <param name="a_position"></param>
/// <param name="a_texture2d"></param>
/// <param name="a_quad"></param>
void main_game_layer::Add_UI_Element(const engine::ref<engine::shader> a_mesh_shader_2d, glm::vec3 a_position, engine::ref<engine::texture_2d> a_texture2d, engine::ref<quad> a_quad)
{
	glm::mat4 transform(1.0f);
	transform = glm::translate(transform, a_position);
	std::dynamic_pointer_cast<engine::gl_shader>(a_mesh_shader_2d)->set_uniform("transparency", 1.0f);
	std::dynamic_pointer_cast<engine::gl_shader>(a_mesh_shader_2d)->set_uniform("has_texture", true);
	a_texture2d->bind();
	engine::renderer::submit(a_mesh_shader_2d, a_quad->mesh(), transform);
}

/// <summary>
/// Creates zombie object
/// </summary>
/// <param name="a_zumbie_props"></param>
/// <returns></returns>
zombie main_game_layer::CreateZombie(engine::game_object_properties a_zumbie_props)
{
	int offset_x = m_mannequin->position().x -10;
	int range_x = m_mannequin->position().x + 10;
	int random_x = offset_x + (rand() % range_x);

	int random = 4 + (rand() % 9);
	int random_z = m_mannequin->position().z - random;

	a_zumbie_props.position = glm::vec3(random_x, a_zumbie_props.position.y, random_z);
	engine::ref<engine::game_object> m_objZ1 = engine::game_object::create(a_zumbie_props);
	engine::bounding_box m_boxZ1;

	m_boxZ1.set_box(a_zumbie_props.bounding_shape.x * a_zumbie_props.scale.x * 5.0f,
		a_zumbie_props.bounding_shape.y * a_zumbie_props.scale.y * 12.0f,
		a_zumbie_props.bounding_shape.z * a_zumbie_props.scale.z * 12.0f,
		a_zumbie_props.position);

	zombie l_zombie{ m_objZ1 , m_boxZ1 };

	return l_zombie;
}
#pragma endregion

