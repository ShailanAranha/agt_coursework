#include "zombie_handler.h"
#include <engine/mouse_buttons_codes.h>
#include <engine/events/key_event.h>
#include <string>


zombie_handler::zombie_handler(){}

zombie_handler::~zombie_handler(){}

/// <summary>
/// Initializes the object via list of zombie instance list
/// </summary>
/// <param name="object"></param>
void zombie_handler::initialise(std::vector<zombie> m_lst_zombies)
{
	m_lstEnemy.push_back(m_enemy1);
	m_lstEnemy.push_back(m_enemy2);
	m_lstEnemy.push_back(m_enemy3);
	m_lstEnemy.push_back(m_enemy4);
	m_lstEnemy.push_back(m_enemy5);

	for (int i = 0; i < m_lstEnemy.size(); i++)
	{
		m_lstEnemy[i].initialise(m_lst_zombies[i].m_object, m_lst_zombies[i].m_object->position(), glm::vec3(1.f, 0.f, 0.f));
		m_lstEnemy[i].m_collider = m_lst_zombies[i].m_collider;
	}
}

/// <summary>
/// Initializes the object via list of enemy instance lsit
/// </summary>
/// <param name="object"></param>
void zombie_handler::initialise2(std::vector<enemy> a_lst_enemy)
{
	m_lstEnemy = a_lst_enemy;
}

/// <summary>
/// Executes ones a frame
/// </summary>
/// <param name="time_step"></param>
/// <param name="a_player"></param>
/// <param name="a_playerBox"></param>
/// <param name="a_playerLastPos"></param>
/// <param name="a_isZombieWave"></param>
/// <param name="a_bulletBox"></param>
/// <param name="a_camera"></param>
void zombie_handler::on_update(const engine::timestep& time_step, player& a_player, engine::bounding_box a_playerBox, glm::vec3 a_playerLastPos, bool& a_isZombieWave, engine::bounding_box& a_bulletBox, engine::perspective_camera& a_camera)
{
	bool m_isHit = false;
	for (int i = 0; i < m_lstEnemy.size(); i++)
	{
		if (m_lstEnemy[i].m_isAlive)
		{

			m_lstEnemy[i].object()->animated_mesh()->on_update(time_step);

			m_lstEnemy[i].on_update(time_step, a_player.object()->position());
			m_lstEnemy[i].m_collider.on_update(m_lstEnemy[i].object()->position());


			if (m_lstEnemy[i].m_collider.collision(a_bulletBox))
			{
				m_lstEnemy[i].m_isAlive = false;
			}

			if (m_lstEnemy[i].m_collider.collision(a_playerBox))
			{
				//a_player.object()->set_position(a_playerLastPos);
				a_player.PushBack(a_camera);
				m_lstEnemy[i].object()->set_position(m_lstEnemy[i].enemy_last_pos);

				a_player.EnemyHit();
				//std::cout << "Player health" << a_player.PlayerHealth();
				a_isZombieWave = true;
				m_timer = 0.0f;
			}

			
		}
	
	}

	if (a_isZombieWave)
	{
		m_timer += time_step;
		if (m_timer > 1)
		{
			a_isZombieWave = false;
		}
	}

	bool l_zombieAliveTest = false;

	for (int i = 0; i < m_lstEnemy.size(); i++)
	{
		if (m_lstEnemy[i].m_isAlive)
		{
			l_zombieAliveTest = true;
		}
	}

	if (l_zombieAliveTest)
	{
		m_isZombieWaveActive = true;
	}
	else
	{
		m_isZombieWaveActive = false;
	}
}

/// <summary>
///  Renders the elements on the screen
/// </summary>
void zombie_handler::on_render(const engine::ref<engine::shader>& mesh_shader, bool a_isColliderVisible, engine::ref<engine::material>	a_zombie_material)
{

	for (int i = 0; i < m_lstEnemy.size(); i++)
	{
		if (m_lstEnemy[i].m_isAlive)
		{
			glm::mat4 zombie_transform(1.0f);
			zombie_transform = glm::translate(zombie_transform, m_lstEnemy[i].object()->position());
			zombie_transform = glm::rotate(zombie_transform, m_lstEnemy[i].object()->rotation_amount(), m_lstEnemy[i].object()->rotation_axis());
			zombie_transform = glm::scale(zombie_transform, m_lstEnemy[i].object()->scale());
			a_zombie_material->submit(mesh_shader);
			engine::renderer::submit(mesh_shader, zombie_transform, m_lstEnemy[i].object());
		}
	}

	if (a_isColliderVisible)
	{
		for (int i = 0; i < m_lstEnemy.size(); i++)
		{
			if (m_lstEnemy[i].m_isAlive)
			{
				m_lstEnemy[i].m_collider.on_render(2.5f, 0.f, 0.f, mesh_shader);

			}
		}
	}
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
void zombie_handler::on_event(engine::event& event, glm::vec3 a_player)
{
	if (event.event_type() == engine::event_type_e::key_pressed)
	{
		auto& e = dynamic_cast<engine::key_pressed_event&>(event);

		// FOR TESTING
		if (e.key_code() == engine::key_codes::KEY_3)
		{
			InitiateZombieWave(a_player);
		}
	
	}
}

/// <summary>
/// cache the current position of all the zombies
/// </summary>
void zombie_handler::cache_zombie_lst_positions()
{
	//std::cout << "cache It started";

	for (int i = 0; i < m_lstEnemy.size(); i++)
	{
		if (m_lstEnemy[i].m_isAlive)
		{
			glm::vec3 enemy_pos = m_lstEnemy[i].object()->position();
			m_lstEnemy[i].enemy_last_pos = enemy_pos;
		}
	}

	//std::cout << "cache It works";
}

/// <summary>
/// Randomise zombuie spawn positions
/// </summary>
void zombie_handler::RandomizeZombiePosition(glm::vec3 a_playerPosition)
{

	for (int i = 0; i < m_lstEnemy.size(); i++)
	{
		//int offset_x = a_playerPosition.x - 15;
		//int range_x = a_playerPosition.x + 15;
		//int random_x = offset_x + (rand() % range_x);

		int offset_x = 0;
		int range_x = 20;
		int random_x = offset_x + (rand() % range_x);

		if (i % 2 == 0)
		{
			//random_x = random_x;
		}
		else
		{
			random_x = -random_x;
		}

		//int offset_x = 1 + (rand() % 9);

		//int random_x = a_playerPosition.x - offset_x;
		//std::cout << offset_x << " "<< range_x<< " "<<  random_x << '\n';
		int random = 4 + (rand() % 9);
		int random_z = a_playerPosition.z - random;
		m_lstEnemy[i].object()->set_position(glm::vec3(random_x, m_lstEnemy[i].object()->position().y, random_z));
	}

	//a_zumbie_props.position = glm::vec3(random_x, a_zumbie_props.position.y, random_z);
}

/// <summary>
/// Start zombie wave
/// </summary>
void zombie_handler::InitiateZombieWave(glm::vec3 a_playerPosition)
{
	RandomizeZombiePosition(a_playerPosition);

	for (int i = 0; i < m_lstEnemy.size(); i++)
	{
		m_lstEnemy[i].m_isAlive = true;
	}

	m_isZombieWaveActive = true;
	
}



