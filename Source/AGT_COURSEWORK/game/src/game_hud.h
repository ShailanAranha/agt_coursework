//HEADER GUARD
#ifndef GAME_HUD
#define GAME_HUD

#include <engine.h>
#include "Gun.h"

#endif

// Forward declaration
class quad;
class Gun;
enum class EPlayerStatus;


class game_hud
{
public:

    /// <summary>
    /// Default constructor
    /// </summary>
	game_hud();
	~game_hud();

    /// <summary>
    /// Initiatize object instance
    /// </summary>
    /// <param name="object"></param>
	void initialise(engine::ref<engine::text_manager>	a_text_manager);

    /// <summary>
    /// Executes once a frame
    /// </summary>
    /// <param name="time_step"></param>
	void on_update(const engine::timestep& time_step);

    /// <summary>
    /// Renders the elements on the screen
    /// </summary>
    /// <param name="shader"></param>
	void on_render(const engine::ref<engine::shader>& shader, bool a_isFirstPersonShooter, bool a_isSafeHouseGateOpen, int a_iPlayerHealth, bool a_isZombieWaveActive);

    /// <summary    >
    /// Event delegate 
    /// </summary>
    /// <param name="event"></param>
    void on_event(engine::event& event);


    // -------------- HELPER FUNCTION -----------

    /// <summary>
    ///  Adds the texture to the quad at the desired tranform and renders in 2D camera
    /// </summary>
    /// <param name="a_mesh_shader_2d"></param>
    /// <param name="a_position"></param>
    /// <param name="a_texture2d"></param>
    /// <param name="a_quad"></param>
    void Add_UI_Element(const engine::ref<engine::shader> a_mesh_shader_2d, glm::vec3 a_position, engine::ref<engine::texture_2d> a_texture2d, engine::ref<quad> a_quad);

    /// <summary>
    /// Current gun selected by the player to shoot
    /// </summary>
    EGunType CurrentGunSelected();

    /// <summary>
    /// Gun 1 instance
    /// </summary>
    Gun m_gun1;

    /// <summary>
    /// Gun 2 instnace
    /// </summary>
    Gun m_gun2;

    /// <summary>
    /// Update distnace info on UI
    /// </summary>
    void updateDistanceFromSafeHouse(int a_iDistance);

    /// <summary>
    /// Change player status based on the current player situation in the game
    /// </summary>
    void ChangeStatus(EPlayerStatus a_status);

    /// <summary>
    /// Current player situation based on defined condition of player in EPlayerStatus
    /// </summary>
    EPlayerStatus CurrentPlayerStatus() { return m_currentPlayerStatus; }

    /// <summary>
    /// 
    /// </summary>
    std::string ZombieWaveDisplayMessage = "";

private:

    /// <summary>
    /// Text manager ref
    /// </summary>
    engine::ref<engine::text_manager>	m_text_manager{};

    /// <summary>
    /// Quad to render background
    /// </summary>
    engine::ref<quad> m_quad;

    /// <summary>
    /// IS distance meter to be rendered?
    /// </summary>
    bool m_is_distance_meter_visible;

    /// <summary>
    /// Distance between safe house and player
    /// </summary>
    int m_iDistanceFromSafeHouse = 0.0f;

    /// <summary>
    /// Current player situation based on defined condition of player in EPlayerStatus
    /// </summary>
    EPlayerStatus m_currentPlayerStatus = EPlayerStatus::LookingForSafeHouse;

};






