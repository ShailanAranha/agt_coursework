//HEADER GUARD
#ifndef GUN_CLASS
#define GUN_CLASS

#include <engine.h>

#endif



class Gun
{
public:

    /// <summary>
    /// Texture for selected status
    /// </summary>
    /// <returns></returns>
    engine::ref<engine::texture_2d> gun_texture_selected() { return m_gun_texture_selected; }

    /// <summary>
    /// Texture for Unselected status
    /// </summary>
    /// <returns></returns>
    engine::ref<engine::texture_2d> gun_texture_unselected() { return m_gun_texture_unselected; }

    /// <summary>
    /// Is Gun selected??
    /// </summary>
    bool m_isGunSelected = false;

    /// <summary>
    /// Is gun picked up by the player?
    /// </summary>
    bool m_isGunPickedUp = false;

    /// <summary>
    /// Default Constructor
    /// </summary>
    Gun() {}
    Gun(std::string a_strSelectedPath, std::string a_strUnselectedPath) :
        m_strSelectedAssetPath(a_strSelectedPath), m_strUnselectedAssetPath(a_strUnselectedPath)
    {
        m_gun_texture_selected = engine::texture_2d::create(a_strSelectedPath, true);
        m_gun_texture_unselected = engine::texture_2d::create(a_strUnselectedPath, true);
    }

    ~Gun()  {   }
private:

    /// <summary>
    /// Asset path for selected gun asset
    /// </summary>
    std::string m_strSelectedAssetPath = "";

    /// <summary>
    /// Asset path for unselected gun asset
    /// </summary>
    std::string m_strUnselectedAssetPath = "";

    /// <summary>
    /// 2D Textures for overlay gun texture
    /// </summary>
    engine::ref<engine::texture_2d> m_gun_texture_selected{};
    engine::ref<engine::texture_2d> m_gun_texture_unselected{};

};


/// <summary>
/// Gun types
/// </summary>
enum class EGunType
{
    Gun1 = 0,
    Gun2 = 1
};

/// <summary>
/// PLayer situations in the game
/// </summary>
enum class EPlayerStatus
{
    LookingForSafeHouse = 0,
    OutsideSafeHouse = 1,
    AtTheRescueAlarm = 2,
    TriggeredRescueAlarm = 3,
    EnteredTheSafeHouse = 4
};
