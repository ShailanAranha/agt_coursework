#include "safe_house_handler.h"
#include <engine/mouse_buttons_codes.h>
#include <engine/events/key_event.h>

/// <summary>
/// Default contructor
/// </summary>
safe_house_handler::safe_house_handler(){}
safe_house_handler::~safe_house_handler(){}

/// <summary>
/// Initializes the object
/// </summary>
/// <param name="object"></param>
void safe_house_handler::initialise(engine::ref<engine::game_object> a_object, float a_fltOpenGatePosition, float a_fltCloseGatePosition)
{
	m_object = a_object;
	m_fltOpenGatePosition = a_fltOpenGatePosition;
	m_fltCloseGatePosition = a_fltCloseGatePosition;
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
/// <param name="shader"></param>
void safe_house_handler::on_render(const engine::ref<engine::shader>& shader, bool a_isFirstPersonShooter)
{
	glm::mat4 l_transform(1.0f);
	l_transform = glm::translate(l_transform, glm::vec3(0.0f, m_object->position().y, m_object->position().z));
	l_transform = glm::rotate(l_transform, m_object->rotation_amount(), m_object->rotation_axis());
	l_transform = glm::scale(l_transform, m_object->scale());

	engine::renderer::submit(shader, l_transform, m_object);


	int l_iCount = 14;

	for (int i = 1; i < l_iCount; i++)
	{
		glm::mat4 l_transform_gate(1.0f);
		l_transform_gate = glm::translate(l_transform_gate, glm::vec3(i * 1.5f, m_object->position().y, m_object->position().z));
		l_transform_gate = glm::rotate(l_transform_gate, m_object->rotation_amount(), m_object->rotation_axis());
		l_transform_gate = glm::scale(l_transform_gate, m_object->scale());

		engine::renderer::submit(shader, l_transform_gate, m_object);


		glm::mat4 l_transform_gate2(1.0f);

		l_transform_gate2 = glm::translate(l_transform_gate2, glm::vec3(-i * 1.5f, m_object->position().y, m_object->position().z));
		l_transform_gate2 = glm::rotate(l_transform_gate2, m_object->rotation_amount(), m_object->rotation_axis());
		l_transform_gate2 = glm::scale(l_transform_gate2, m_object->scale());

		engine::renderer::submit(shader, l_transform_gate2, m_object);

	}
}

/// <summary>
/// Executes ones a frame
/// </summary>
/// <param name="time_step"></param>
void safe_house_handler::on_update(const engine::timestep& time_step)
{
	if (m_isGateOpen)
	{

		if (m_fltMovement > m_fltOpenGatePosition)
		{
			m_fltMovement -= time_step * m_fltSpeed;
		}
	}
	else
	{
		if (m_fltMovement < m_fltCloseGatePosition)
		{
			//std::cout << "m_fltMovement before" << m_fltMovement;
			m_fltMovement += time_step * m_fltSpeed;
			//std::cout << "m_fltMovement " << m_fltMovement;
		}
	}


	m_object->set_position(glm::vec3(m_object->position().x, m_fltMovement, m_object->position().z));

}

/// <summary>
/// Open safe house gate
/// </summary>
void safe_house_handler::OpenSafeHouseGate()
{
	m_isGateOpen = true;
}

/// <summary>
/// Close safe house gate
/// </summary>
void safe_house_handler::CloseSafeHouseGate()
{
	m_isGateOpen = false;
}

/// <summary>
/// Renders the elements on the screen
/// </summary>
void safe_house_handler::on_event(engine::event& event)
{
	if (event.event_type() == engine::event_type_e::key_pressed)
	{
		auto& e = dynamic_cast<engine::key_pressed_event&>(event);

		// FOR TESTING
		/*if (e.key_code() == engine::key_codes::KEY_O)
		{
			OpenSafeHouseGate();
		}

		if (e.key_code() == engine::key_codes::KEY_P)
		{
			CloseSafeHouseGate();
		}*/
	}

}

